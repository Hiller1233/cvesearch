﻿using System;

namespace CveSearchMain.Logger
{
    public class LoggerBase : ILogger
    {
        public LoggerBase() : this(null)
        { }

        public LoggerBase(ILogger? upperLogger)
        {
            UpperLogger = upperLogger;
        }

        public virtual void Log(string message, LogSeverity severity) => UpperLogger?.Log(message, severity);

        public void Log(string message) => Log(message, LogSeverity.Info);

        public void Debug(string message) => Log(message, LogSeverity.Debug);

        public StopProgramException LogAndCreateStopProgramException(string message) => 
            LogAndCreateStopProgramException(message, null, LogSeverity.Error);

        public StopProgramException LogAndCreateStopProgramException(string message, LogSeverity severity) 
            => LogAndCreateStopProgramException(message, null, LogSeverity.Error);

        public StopProgramException LogAndCreateStopProgramException(string message, Exception? innerException) 
            => LogAndCreateStopProgramException(message, innerException, LogSeverity.Error);

        public StopProgramException LogAndCreateStopProgramException(string message, Exception? innerException, LogSeverity severity)
        {
            Log(message, severity);
            return new StopProgramException(message, innerException);
        }

        public virtual ILogger? UpperLogger { get; }
    }
}