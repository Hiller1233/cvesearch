﻿namespace CveSearchMain.Logger
{
    public enum LogSeverity
    {
        Debug,
        Info,
        Success,
        Error,
        RuntimeInfo
    }
}