﻿using System.Text;

namespace CveSearchMain.Logger
{
    public class PatchInfoLogger : LoggerBase
    {
        private readonly StringBuilder sb;

        public PatchInfoLogger() : this(null)
        { }

        public PatchInfoLogger(ILogger? upperLogger) : base(upperLogger)
        {
            sb = new StringBuilder();
        }

        public string GetContent() => sb.ToString();
        
        public override void Log(string message, LogSeverity severity)
        {
            if (severity == LogSeverity.RuntimeInfo)
            {
                base.Log(message, severity);
                return;
            }
            sb.AppendLine(severity == LogSeverity.Error 
                ? $"<li class=\"error-text\">{message}</li>"
                : $"<li>{message}</li>");
            base.Log(message, severity);
        }
    }
}