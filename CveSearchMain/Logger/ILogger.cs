﻿using System;

namespace CveSearchMain.Logger
{
    public interface ILogger
    {
        void Log(string message);
        void Log(string message, LogSeverity severity);

        void Debug(string message);
        
        StopProgramException LogAndCreateStopProgramException(string message);
        StopProgramException LogAndCreateStopProgramException(string message, LogSeverity severity);
        StopProgramException LogAndCreateStopProgramException(string message, Exception? innerException);
        StopProgramException LogAndCreateStopProgramException(string message, Exception? innerException, LogSeverity severity);
        
        ILogger? UpperLogger { get; } 
    }
}