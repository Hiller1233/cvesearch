﻿using System;
using System.Diagnostics.CodeAnalysis;
#pragma warning disable 162

namespace CveSearchMain.Logger
{
    public class ConsoleLogger : LoggerBase
    {
        public ConsoleLogger() : this(null)
        { }

        public ConsoleLogger(ILogger? upperLogger) : base(upperLogger)
        { }
        
        [SuppressMessage("ReSharper", "HeuristicUnreachableCode")]
        public override void Log(string message, LogSeverity severity)
        {
            var pref = "";
            switch (severity)
            {
                case LogSeverity.Debug:
                    if (Program.IsDebug)
                    {
                        pref = "[D] ";
                        break;
                    }

                    base.Log(message, severity);
                    return;
                case LogSeverity.Info:
                case LogSeverity.RuntimeInfo:
                case LogSeverity.Success:
                    break;
                case LogSeverity.Error:
                    pref = "** ";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(severity), severity, null);
            }
            Console.WriteLine(pref + message);
            base.Log(message, severity);
        }
    }
}