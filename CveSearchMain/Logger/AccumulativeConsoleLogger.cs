﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
#pragma warning disable 162

namespace CveSearchMain.Logger
{
    public class AccumulativeConsoleLogger : LoggerBase
    {
        private readonly StringBuilder sb;

        public AccumulativeConsoleLogger(ILogger upperLogger) : base(upperLogger)
        {
            sb = new StringBuilder();
        }

        public void Flush(string firstLine)
        {
            UpperLogger?.Log(firstLine + sb);
        }

        [SuppressMessage("ReSharper", "HeuristicUnreachableCode")]
        public override void Log(string message, LogSeverity severity)
        {
            var pref = "";
            switch (severity)
            {
                case LogSeverity.Debug:
                    if (Program.IsDebug)
                    {
                        pref = "[D] ";
                        break;
                    }

                    base.Log(message, severity);
                    return;
                case LogSeverity.Info:
                case LogSeverity.Success:
                    break;
                case LogSeverity.Error:
                    pref = "** ";
                    break;
                case LogSeverity.RuntimeInfo:
                    base.Log(message, severity);
                    return;
                default:
                    throw new ArgumentOutOfRangeException(nameof(severity), severity, null);
            }

            sb.AppendLine().Append(pref + message);
        }
    }
}