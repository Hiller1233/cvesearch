﻿using System;

namespace CveSearchMain
{
    public class StopProgramException : Exception
    {
        public StopProgramException(string message) : base(message)
        { }
        
        public StopProgramException(string message, Exception? innerException) : base(message, innerException)
        { }
    }
}