﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using CveSearchMain.InputParser.ArgCommand;

namespace CveSearchMain.InputParser
{
    public class AutoCommandCollection<TArgs> : CommandCollection
        where TArgs : IInputArgs, new()
    {
        private static readonly Dictionary<string, ArgCommandBase> typeCommandByName;
        private static readonly Func<Dictionary<string, object>, TArgs> typeLambda;

        static AutoCommandCollection()
        {
            var parameters = typeof(TArgs).GetProperties()
                .Select(p => (attr: p.GetCustomAttribute<CommandInfoAttribute>(), p))
                .Where(tup => tup.attr != null && tup.p.CanWrite)
                .ToArray();
            typeCommandByName = new Dictionary<string, ArgCommandBase>();
            FillDictionary(typeCommandByName, parameters.Select(tup =>  ArgCommandFactory.Create(tup.attr!, tup.p.PropertyType)));
            typeLambda = ExpressionsMethods.CreateAccessLambda(typeof(TArgs), parameters!);
        }

        public AutoCommandCollection(bool reserveHelp=true)
            : base(new Dictionary<string, ArgCommandBase>(typeCommandByName), reserveHelp)
        {
            if (reserveHelp)
                ReserveHelpCommand();
        }

        public override void AddCommand(ArgCommandBase cmd)
        {
            if (cmd is HelpArgCommand || cmd is ExecutableArgCommand)
            {
                base.AddCommand(cmd);
            }
            else
                throw new ArgumentException("Поддерживаются только исполняемые команды");
        }
        
        public void AddCommand(HelpArgCommand cmd) => base.AddCommand(cmd);
        
        public void AddCommand(ExecutableArgCommand cmd) => base.AddCommand(cmd);

        public TArgs GetArgsFromDictionary(Dictionary<string, object> dict) => typeLambda(dict);

        private static class ExpressionsMethods
        {
            public static Func<Dictionary<string, object>, TArgs> CreateAccessLambda(Type type,
                IEnumerable<(CommandInfoAttribute, PropertyInfo)> parameters)
            {
                var rawLambda = CreateRawLambda(type, parameters);
                var lambda = rawLambda.Compile();
                return lambda;
            }

            private static Expression<Func<Dictionary<string, object>, TArgs>> CreateRawLambda(Type type,
                IEnumerable<(CommandInfoAttribute, PropertyInfo)> parameters)
            {
                var containsKey = typeof(Dictionary<string, object>).GetMethod("ContainsKey")
                                  ?? throw new ArgumentNullException();
                var dict = Expression.Parameter(typeof(Dictionary<string, object>), "Dictionary");
                var bindings = new List<MemberBinding>();
                foreach (var (arg, prop) in parameters)
                {
                    var key = Expression.Constant(arg.FullName);
                    var containsResult = Expression.Call(dict, containsKey, key);
                    var getDictionaryValue = Expression.Convert(Expression.Property(dict, "Item", key),
                        prop.PropertyType);
                    var getDefault = Expression.Default(prop.PropertyType);
                    var getValue = Expression.Condition(containsResult, getDictionaryValue, getDefault);
                    var bind = Expression.Bind(prop, getValue);
                    bindings.Add(bind);
                }

                var body = Expression.MemberInit(Expression.New(type), bindings);

                var rawLambda = Expression.Lambda<Func<Dictionary<string, object>, TArgs>>(body, dict);
                return rawLambda;
            }
        }
    }
}