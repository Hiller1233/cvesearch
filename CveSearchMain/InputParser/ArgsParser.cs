﻿using System;
using System.Collections.Generic;

namespace CveSearchMain.InputParser
{
    public class ArgsParser
    {
        protected readonly CommandCollection Commands;

        public ArgsParser(CommandCollection commands)
        {
            Commands = commands;
        }

        public Dictionary<string, object>? ParseArgsRaw(string[] args)
        {
            var dict = new Dictionary<string, object>();
            var list = new List<string>();
            var q = -1;
            for (var i = 0; i < args.Length; i++)
            {
                var s = args[i];
                if (q == -1 && s.StartsWith('\"')) 
                    q = i;

                if (q != -1 && s.EndsWith('\"'))
                {
                    s = string.Join(' ', args[q..i])[1..^1];
                    q = -1;
                }
                
                if (q == -1)
                    list.Add(s);
            }
            
            if (q != -1)
                list.Add(string.Join(' ', args[q..])[1..]);

            if (args.Length != list.Count)
                args = list.ToArray();

            for (var it = 0; it < args.Length; it++)
            {
                if (!Commands.FindCommand(args[it], out var cmd))
                    throw new StopProgramException($"** Команда или параметр {args[it]} не найдена");
                if (dict.ContainsKey(cmd.FullName))
                    throw new StopProgramException($"** Параметр {cmd.FullName} дублируется");
                if (Commands.IsHelpReserved && cmd.FullName == "--help")
                {
                    Console.WriteLine(Commands.GetHelpMessage());
                    return null;
                }

                var offset = cmd.ProcessCommand(dict, args[(it+1)..]);
                if (offset == -1)
                    return null;
                it += offset;
            }
                
            return dict;
        }
    }
}