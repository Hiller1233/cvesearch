﻿using System;
using CveSearchMain.InputParser.ArgCommand;

namespace CveSearchMain.InputParser
{
    public static class ArgCommandFactory
    {
        public static ArgCommandBase Create(string fullName, string shortName, string description, bool helpCommand=false)
        {
            return helpCommand
                ? (ArgCommandBase) new HelpArgCommand(fullName, shortName, description)
                : new ParameterizedArgCommand(fullName, shortName, description);
        }
        
        public static ArgCommandBase Create(string fullName, string shortName, string description, int numberOfParameters)
        {
            return new ParameterizedArgCommand(fullName, shortName, description, numberOfParameters);
        }
        
        public static ArgCommandBase Create(string fullName, string shortName, string description, Type typeOfProperty)
        {
            return new ConvertibleArgCommand(fullName, shortName, description, typeOfProperty);
        }
        
        public static ArgCommandBase Create(string fullName, string shortName, string description, Action<string[]> action)
        {
            return new ExecutableArgCommand(fullName, shortName, description, action);
        }
        
        public static ArgCommandBase Create(CommandInfoAttribute commandInfo, bool helpCommand=false) 
            => Create(commandInfo.FullName, commandInfo.ShortName, commandInfo.Description, helpCommand);

        public static ArgCommandBase Create(CommandInfoAttribute commandInfo, int numberOfParameters) 
            => Create(commandInfo.FullName, commandInfo.ShortName, commandInfo.Description, numberOfParameters);

        public static ArgCommandBase Create(CommandInfoAttribute commandInfo, Type typeOfProperty) 
            => Create(commandInfo.FullName, commandInfo.ShortName, commandInfo.Description, typeOfProperty);

        public static ArgCommandBase Create(CommandInfoAttribute commandInfo, Action<string[]> action) 
            => Create(commandInfo.FullName, commandInfo.ShortName, commandInfo.Description, action);
    }
}