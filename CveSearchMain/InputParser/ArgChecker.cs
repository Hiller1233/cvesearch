﻿using System;
using System.Collections.Generic;

namespace CveSearchMain.InputParser
{
    public static class ArgChecker
    {
        private static readonly Dictionary<Type, Func<object, object>> knownTypes;

        static ArgChecker()
        {
            knownTypes = new Dictionary<Type, Func<object, object>>()
            {
                [typeof(int)] = o => ParseInt(o),
                [typeof(int[])] = o => ParseIntArray(o),
                [typeof(string)] = o => o,
                [typeof(string[])] = o => o,
                [typeof(double)] = o => ParseDouble(o),
                [typeof(double[])] = o => ParseDoubleArray(o),
                [typeof(int?)] = o => ParseInt(o),
                [typeof(DateTimeOffset)] = o => ParseDateTimeOffset(o),
                [typeof(DateTimeOffset?)] = o => ParseDateTimeOffset(o),
                [typeof(DateTime)] = o => ParseDateTime(o),
                [typeof(DateTime?)] = o => ParseDateTime(o)
            };
        }
        
        public static DateTimeOffset ParseDateTimeOffset(object input)
        {
            if (!DateTimeOffset.TryParse(input.ToString(), out var result))
                throw new StopProgramException($"** Не удалось преобразовать {input} к дате");
            return result;
        }
        
        public static DateTime ParseDateTime(object input)
        {
            if (!DateTime.TryParse(input.ToString(), out var result))
                throw new StopProgramException($"** Не удалось преобразовать {input} к дате");
            return result;
        }
        
        public static int ParseInt(object input)
        {
            if (!int.TryParse(input.ToString(), out var result))
                throw new StopProgramException($"** Не удалось преобразовать {input} к целочисленному числу");
            return result;
        }

        public static double ParseDouble(object input)
        {
            if (!double.TryParse(input.ToString(), out var result))
                throw new StopProgramException($"** Не удалось преобразовать {input} к вещественному числу");
            return result;
        }

        public static int[] ParseIntArray(object input)
        {
            return Array.ConvertAll(CheckArray(input), ParseInt);
        }

        public static double[] ParseDoubleArray(object input)
        {
            return Array.ConvertAll(CheckArray(input), ParseDouble);
        }
        
        public static Func<object, object> GetConverter(Type typeOfProperty)
        {
            if (knownTypes.TryGetValue(typeOfProperty, out var func))
                return func;
            throw new ArgumentOutOfRangeException($"Тип {typeOfProperty.Name} не поддерживается");
        }
        
        private static string[] CheckArray(object input)
        {
            if (!(input is string[] arr))
                throw new StopProgramException($"** Не удалось получить данные из коллекции");
            return arr;
        }
    }
}