﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CveSearchMain.InputParser.ArgCommand;

namespace CveSearchMain.InputParser
{
    public class CommandCollection
    {
        protected readonly Dictionary<string, ArgCommandBase> CommandByName;
        private string? helpMessage;

        public CommandCollection(params ArgCommandBase[] commands) : this(true, commands)
        {
            
        }
        
        public CommandCollection(bool reserveHelp=true, params ArgCommandBase[] commands)
        {
            CommandByName = new Dictionary<string, ArgCommandBase>();
            FillDictionary(CommandByName, commands);
            if (reserveHelp)
                ReserveHelpCommand();
            IsHelpReserved = reserveHelp;
        }
        
        protected CommandCollection(Dictionary<string, ArgCommandBase> staticCommands, bool reserveHelp=true)
        {
            CommandByName = staticCommands;
            IsHelpReserved = reserveHelp;
        }

        private static string CreateHelpMessage(IEnumerable<ArgCommandBase> commands)
        {
            var sb = new StringBuilder();
            
            foreach (var cmd in commands)
                sb.AppendLine($"{cmd.ShortName}, {cmd.FullName}: {cmd.Description}");

            return sb.ToString();
        }

        protected static void FillDictionary(IDictionary<string, ArgCommandBase> dict, IEnumerable<ArgCommandBase> commands)
        {
            foreach (var cmd in commands)
            {
                dict.Add(cmd.FullName, cmd);
                dict.Add(cmd.ShortName, cmd);
            }
        }

        protected void ReserveHelpCommand()
        {
            if (CommandByName.ContainsKey("--help") || CommandByName.ContainsKey("-h"))
                throw new ArgumentException("Предоставленная коллекция команд уже содержит команду --help/-h");
            var helpCmd = new HelpArgCommand("--help", "-h", "вывод справочного сообщения", this);
            CommandByName.Add(helpCmd.FullName, helpCmd);
            CommandByName.Add(helpCmd.ShortName, helpCmd);
        }

        public virtual void AddCommand(ArgCommandBase cmd)
        {
            CommandByName.Add(cmd.FullName, cmd);
            CommandByName.Add(cmd.ShortName, cmd);
        }

        public string? GetHelpMessage() => helpMessage ??= CreateHelpMessage(CommandByName.Values.Distinct());

        public bool IsHelpReserved { get; }

        public ArgCommandBase? GetCommand(string name) => CommandByName.ContainsKey(name) ? CommandByName[name] : null;

        public bool FindCommand(string name, out ArgCommandBase cmd)
        {
            cmd = GetCommand(name)!;
            return cmd != null;
        }
    }
}