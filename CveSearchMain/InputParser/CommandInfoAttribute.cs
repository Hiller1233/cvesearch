﻿using System;

namespace CveSearchMain.InputParser
{
    public class CommandInfoAttribute : Attribute
    {
        public CommandInfoAttribute(string fullName, string shortName, string description)
        {
            FullName = fullName;
            ShortName = shortName;
            Description = description;
        }

        public string FullName { get; }
        public string ShortName { get; }
        public string Description { get; }
    }
}