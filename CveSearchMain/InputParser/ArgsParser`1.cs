﻿namespace CveSearchMain.InputParser
{
    public class ArgsParser<TArgs> : ArgsParser
        where TArgs : class, IInputArgs, new()
    {
        private static readonly AutoCommandCollection<TArgs> typeCommands;

        static ArgsParser()
        {
            typeCommands = new AutoCommandCollection<TArgs>();
        }
        
        public ArgsParser() : base(typeCommands)
        {
            
        }

        public TArgs? ParseArgs(string[] args)
        {
            var dict = ParseArgsRaw(args);
            return dict != null ? typeCommands.GetArgsFromDictionary(dict) : null;
        }
    }
}