﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CveSearchMain.InputParser.ArgCommand
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
    public class HelpArgCommand : ArgCommandBase
    {
        private CommandCollection? targetCollection;

        public HelpArgCommand(string fullName, string shortName, string description, 
            CommandCollection targetCollection=null!) : base(fullName, shortName, description)
        {
            this.targetCollection = targetCollection;
            CommandType = ArgCommandType.Help;
        }

        public void SetCommandCollection(CommandCollection collection)
        {
            targetCollection = collection;
        }

         public void PrintHelpMessage()
        {
            Console.WriteLine(targetCollection == null ? "В данный момент справка не доступна" 
                : targetCollection.GetHelpMessage());
        }
         
         public override int ProcessCommand(Dictionary<string, object> dictToFill, string[] args)
         {
             PrintHelpMessage();
             return -1;
         }
    }
}