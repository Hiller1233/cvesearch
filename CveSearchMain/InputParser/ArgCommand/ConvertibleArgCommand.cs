﻿using System;
using System.Collections.Generic;

namespace CveSearchMain.InputParser.ArgCommand
{
    public class ConvertibleArgCommand : ParameterizedArgCommand
    {
        private readonly Func<object, object>? convert;

        public ConvertibleArgCommand(string fullName, string shortName, string description, Type typeOfProperty) 
            : base(fullName, shortName, description)
        {
            if (typeOfProperty == typeof(bool))
                CommandType = ArgCommandType.Option;
            else
                CommandType = typeOfProperty.IsArray ? ArgCommandType.Collection : ArgCommandType.Parameter;

            if (CommandType == ArgCommandType.Option)
                return;

            convert = ArgChecker.GetConverter(typeOfProperty);
        }

        public object ConvertInput(object input) => convert?.Invoke(input)!;
        
        public override int ProcessCommand(Dictionary<string, object> dictToFill, string[] args) 
            => ProcessCommandInternal(dictToFill, args, ConvertInput);
    }
}