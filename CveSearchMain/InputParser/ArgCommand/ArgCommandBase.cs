﻿using System.Collections.Generic;

namespace CveSearchMain.InputParser.ArgCommand
{
    public abstract class ArgCommandBase
    {
        protected ArgCommandBase(string fullName, string shortName, string description)
        {
            FullName = fullName;
            ShortName = shortName;
            Description = description;
        }

        public abstract int ProcessCommand(Dictionary<string, object> dictToFill, string[] args);

        public string FullName { get; }
        public string ShortName { get; }
        public string Description { get; }
        public ArgCommandType CommandType { get; protected set; }

        public enum ArgCommandType
        {
            Option,
            Collection,
            Parameter,
            Action,
            Help
        }
    }
}