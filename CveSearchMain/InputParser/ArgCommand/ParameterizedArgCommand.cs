﻿using System;
using System.Collections.Generic;

namespace CveSearchMain.InputParser.ArgCommand
{
    public class ParameterizedArgCommand : ArgCommandBase
    {
        public ParameterizedArgCommand(string fullName, string shortName, string description, int numberOfParameters) 
            : base(fullName, shortName, description)
        {
            if (numberOfParameters >= 0)
                CommandType = numberOfParameters == 0 ? ArgCommandType.Option : ArgCommandType.Parameter;
            else
                CommandType = ArgCommandType.Collection;
        }

        public ParameterizedArgCommand(string fullName, string shortName, string description) 
            : base(fullName, shortName, description)
        {
            CommandType = ArgCommandType.Option;
        }
        
        public override int ProcessCommand(Dictionary<string, object> dictToFill, string[] args) 
            => ProcessCommandInternal(dictToFill, args, o => o);

        protected int ProcessCommandInternal(Dictionary<string, object> dictToFill, string[] args, 
            Func<object, object> transformation)
        {
            var offset = 0;
            
            switch (CommandType)
            {
                case ArgCommandType.Option:
                    dictToFill.Add(FullName, true);
                    break;
                case ArgCommandType.Collection:
                    dictToFill.Add(FullName, transformation(args));
                    offset = args.Length;
                    break;
                case ArgCommandType.Parameter:
                    if (args.Length < 1)
                        throw new StopProgramException
                            ($"** Параметр {FullName} должен принимать 1 аргумент");
                    dictToFill.Add(FullName, transformation(args[0]));
                    offset = 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return offset;
        }
    }
}