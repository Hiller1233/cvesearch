﻿using System;
using System.Collections.Generic;

namespace CveSearchMain.InputParser.ArgCommand
{
    public class ExecutableArgCommand : ArgCommandBase
    {
        private readonly Action<string[]> action;

        public ExecutableArgCommand(string fullName, string shortName, string description, Action<string[]> action) 
            : base(fullName, shortName, description)
        {
            this.action = action;
            CommandType = ArgCommandType.Action;
        }

        public void ExecuteCommand(string[] args) => action.Invoke(args);
        
        public override int ProcessCommand(Dictionary<string, object> dictToFill, string[] args)
        {
            ExecuteCommand(args);
            return -1;
        }
    }
}