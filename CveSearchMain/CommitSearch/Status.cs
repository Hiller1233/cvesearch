﻿namespace CveSearchMain.CommitSearch
{
    public enum Status
    {
        NotFound,
        FoundId,
        FoundDate,
        FoundMessage,
        Error
    }
}