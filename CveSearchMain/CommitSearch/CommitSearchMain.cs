﻿using System;
using System.Collections.Generic;
using CveSearchMain.CommitSearch.RepositoryWorker;
using CveSearchMain.CommitSearch.Search;
using CveSearchMain.CveAnalyzer;
using CveSearchMain.InputParser;
using CveSearchMain.Logger;
using CveSearchMain.NvdDownloader;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch
{
    public class CommitSearchMain : IModule
    {
        private readonly ILogger logger;
        private ArgsParser<Args>? argsParser;

        private static string DefaultTempFolder => @"temp";
        private static string DefaultRepositoryWorker => "db";
        
        public CommitSearchMain(ILogger logger)
        {
            this.logger = logger;
        }
        
        public IReadOnlyList<CommitSearchResult>? StartCommitSearch(Args args)
        {
            ValidateAndFixArgs(args);
            List<CommitSearchResult>? result;
            IRepositoryWorker repositoryWorker = null!;
            try
            {
                var repo = new Repository(args.PathToRepo);
                repositoryWorker = GetWorker(repo, args);
                if (args.UseRequestCache)
                    NvdDownloaderMain.PrepareCache(logger, args.LoadRequests, args.Keywords, args.TempFolder!);
                logger.Log("Начало поиска в репозитории");
                var search = GetSearch(repositoryWorker, args, out result);
                search.Start();
                if (args.SaveRequests)
                    NvdDownloaderMain.SaveCache(logger);
            }
            catch (StopProgramException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw logger
                    .LogAndCreateStopProgramException($"Не удалось открыть репозиторий по пути {args.PathToRepo}", e);
            }
            finally
            {
                if (repositoryWorker is SqliteRepositoryWorker rw)
                    rw?.Dispose();
            }
            
            if (result != null && !args.DoNotPrintResultsToConsole)
                foreach (var r in result)
                    PrintToConsole(r);

            return result;
        }

        private static void PrintToConsole(CommitSearchResult result)
        {
            Console.WriteLine(result.CommitId + ":");
            Console.WriteLine(result.Message);
        }

        private IRepositoryWorker GetWorker(Repository repo, Args args)
        {
            switch (args.RepositoryWorker)
            {
                case "db": 
                    return new SqliteRepositoryWorker(logger, repo, args.DeepCompare, args.TempFolder!, 
                        !args.DoNotLoadCacheFromFile);
                case "file":
                    return new GitRepositoryWorker(repo, args.DeepCompare);
                case "cache": 
                    var cache = BuildCache(repo, args);
                    return new CacheRepositoryWorker(repo, args.DeepCompare, cache);
                default:
                    throw new ArgumentException();
            }
        }
        
        private ISearch GetSearch(IRepositoryWorker repositoryWorker, Args args,
            out List<CommitSearchResult>? result)
        {
            if (args.AnalyzerEntries == null)
            {
                result = new List<CommitSearchResult>();
                return new LinkSearch(logger, repositoryWorker, args.LinksToCommit!, result, args.StopProgramOnError);
            }

            result = null;
            return !args.DoNotUseParallelSearch 
                ? new ParallelPatchSearch(logger, repositoryWorker, args.AnalyzerEntries!) 
                : new PatchSearch(logger, repositoryWorker, args.AnalyzerEntries, args.StopProgramOnError);
        }


        
        private RepositoryCache.Collection BuildCache(Repository repo, Args args)
        {
            try
            {
                return RepositoryCache.BuildRepositoryCache(logger, repo, args.TempFolder!, !args.DoNotSaveCacheToFile, 
                    !args.DoNotLoadCacheFromFile);
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException("Не удалось создать кэш репозитория", e);
            }
        }

        private void ValidateAndFixArgs(Args args)
        {
            if (args.AnalyzerEntries != null)
                if (args.AnalyzerEntries.Count == 0)
                    args.AnalyzerEntries = null;
                else
                    args.LinksToCommit = null;
            if (args.LinksToCommit == null && args.AnalyzerEntries == null)
                throw new StopProgramException("** Ссылки на коммиты должны быть заданы явно");
            if (args.PathToRepo == null)
                throw new StopProgramException("** Путь к репозиторию должен быть задан явно");
            if (args.StopProgramOnError && !args.DoNotUseParallelSearch)
                throw new StopProgramException("** невозможно использовать параллельный поиск и остановку " +
                                               "программы при ошибке");
            args.TempFolder ??= DefaultTempFolder;
            args.RepositoryWorker ??= DefaultRepositoryWorker;
            if (args.RepositoryWorker != "db" && args.RepositoryWorker != "cache" 
                                         && args.RepositoryWorker != "file")
                throw logger.LogAndCreateStopProgramException("Обработчик репозитория имеет неверное значение");
        }

        public void ProcessArgs(string[] args)
        {
            argsParser ??= new ArgsParser<Args>();
            var commitSearchArgs = argsParser.ParseArgs(args);
            if (commitSearchArgs != null)
                StartCommitSearch(commitSearchArgs);
        }

        public class Args : IInputArgs
        {
            [CommandInfo("--path", "-p", "путь к репозиторию git")]
            public string? PathToRepo { get; set; }
            
            [CommandInfo("--do-not-print", "-np", "не выводить найденные коммиты в консоль")]
            public bool DoNotPrintResultsToConsole { get; set; }
            
            [CommandInfo("--stop-on-error", "-s", "при ошибке при поиске коммита программа остановится")]
            public bool StopProgramOnError { get; set; }
            
            [CommandInfo("--deep-compare", "-dp", "использовать полный и медленный поиск по теме")]
            public bool DeepCompare  { get; set; }

            [CommandInfo("--do-not-save-cache", "-ncs", "не сохранять кэш репозитория в файл (не применяется к БД)")]
            public bool DoNotSaveCacheToFile  { get; set; }
            
            [CommandInfo("--do-not-load-cache", "-ncl", "не загружать кэш/БД репозитория из файла")]
            public bool DoNotLoadCacheFromFile  { get; set; }
            
            [CommandInfo("--dir", "-d", "путь к директории для сохранения файлов")]
            public string? TempFolder  { get; set; }
            
            [CommandInfo("--links", "-l", "ссылки на искомые коммиты (аргумент вводится последним)")]
            public string[]? LinksToCommit { get; set; }
            
            [CommandInfo("--do-not-use-parallel-search", "-nps", "не использовать параллельный поиск коммитов")]
            public bool DoNotUseParallelSearch  { get; set; }
            
            [CommandInfo("--repository-worker", "-rw", "обработчик репозитория ([db], cache, file)")]
            public string? RepositoryWorker { get; set; }

            public bool SaveRequests  { get; set; }
            public bool LoadRequests  { get; set; }
            public bool UseRequestCache { get; set; }
            public IReadOnlyList<CveAnalyzerResult>? AnalyzerEntries { get; set; }
            public string[]? Keywords { get; set; }
        }
    }
}