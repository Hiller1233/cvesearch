﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using CveSearchMain.CommitSearch.PatchParser;
using CveSearchMain.Logger;
using LibGit2Sharp;
using Microsoft.Data.Sqlite;

namespace CveSearchMain.CommitSearch.RepositoryWorker
{
    public class SqliteRepositoryWorker : RepositoryWorkerBase, IDisposable
    {
        private readonly ILogger logger;
        private readonly SqliteConnection longLifeConnection;
        
        public SqliteRepositoryWorker(ILogger logger, Repository repo, bool deepCompare, string tempFolder, 
            bool loadFromFile) : base(repo, deepCompare)
        {
            this.logger = logger;

            string dbName = Repo.Head.Tip.Sha + "-db.db3";
            var dbFile = new FileInfo(Path.Join(tempFolder, dbName));
            if (!loadFromFile || !dbFile.Exists)
                CreateDb(dbFile);

            longLifeConnection = new SqliteConnection("Mode=ReadOnly;Data Source=" + dbFile.FullName);
            longLifeConnection.Open();
        }

        private static void CreateTables(SqliteConnection connection)
        {
            const string text = @"
CREATE TABLE commits
(
    id      TEXT PRIMARY KEY NOT NULL,
    subject TEXT             NOT NULL,
    date    TEXT             NOT NULL
);
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = text;
            cmd.ExecuteNonQuery();
        }
        
        private static void AddCommit(SqliteConnection connection, Commit commit)
        {
            const string text = @"
INSERT INTO commits (id, subject, date)
VALUES ('{0}', '{1}', '{2}');
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = string.Format(text, commit.Sha, commit.MessageShort.Replace("\'", "\'\'"), 
                commit.Author.When.ToUniversalTime().ToString("yyyy-MM-dd HH-mm-ss"));
            cmd.ExecuteNonQuery();
        }
        
        private List<ObjectId>? SelectSubjectDeepCompare(SqliteConnection connection, string where)
        {
            const string text = @"
SELECT id FROM commits
WHERE {0};
";
            return Execute(connection, string.Format(text, where));
        }

        private List<ObjectId>? SelectSubject(SqliteConnection connection, string subject)
        {
            const string text = @"
SELECT id FROM commits
WHERE subject='{0}';
";
            return Execute(connection, string.Format(text, subject.Replace("\'", "\'\'")));
        }
        
        private List<ObjectId>? SelectDate(SqliteConnection connection, DateTimeOffset date)
        {
            const string text = @"
SELECT id FROM commits
WHERE date='{0}';
";
            return Execute(connection, string.Format(text, date.ToUniversalTime().ToString("yyyy-MM-dd HH-mm-ss")));
        }

        private List<ObjectId>? Execute(SqliteConnection connection, string text)
        {
            using var cmd = connection.CreateCommand();
            cmd.CommandText = text;
            using var reader = cmd.ExecuteReader();
            if (!reader.HasRows)
                return null;
            
            var res = new List<ObjectId>();
            foreach (DbDataRecord? entry in reader)
            {
                try
                {
                    var id = entry!.GetString(0);
                    res.Add(new ObjectId(id));
                }
                catch (Exception e)
                {
                    var msg = "Ошибка при чтении из БД репозитория";
                    throw logger.LogAndCreateStopProgramException(msg, e);
                }
            }

            return res;
        }
        
        private IEnumerable<Commit> FindCommitsBySubject(string targetMessage)
        {
            var idArr = SelectSubject(longLifeConnection!, targetMessage);
            if (idArr == null)
                yield break;
            foreach (var commit in idArr)
                yield return RepoLookup(commit)!;
        }
        
        private IEnumerable<Commit> FindCommitsByDate(DateTimeOffset targetDate)
        {
            var idArr = SelectDate(longLifeConnection!, targetDate);
            if (idArr == null)
                yield break;
            
            foreach (var commit in idArr)
                yield return RepoLookup(commit)!;
        }

        private void EndDbCreating(SqliteConnection connection, FileInfo file)
        {
            logger.Log("Создание БД репозитория завершено");
            try
            {
                if (file.Exists)
                {
                    file.Delete();
                    logger.Debug($"Удален старый файл {file.Name}");
                }
                
                if (!file.Directory.Exists)
                    file.Directory.Create();
                
                using var backup = new SqliteConnection("Data Source=" + file.FullName);
                backup.Open();
                connection.BackupDatabase(backup);
                logger.Log($"БД репозитория сохранена в файл {file.FullName}");
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException($"Не удалось сохранить БД в файл {file.FullName}", e);
            }
        }

        protected void FillDbFromRepo(SqliteConnection connection)
        {
            logger.Log("Чтение из репозитория");
            foreach (var commit in Repo.Commits) 
                AddCommit(connection, commit);
        }

        private void CreateDb(FileInfo dbFile)
        {
            try
            {
                logger.Log("Создание БД репозитория");
                using var connection = new SqliteConnection("Data Source=Sharable;Mode=Memory");
                connection.Open();
                CreateTables(connection);
                logger.Debug("Завершено создание основных таблиц");
                FillDbFromRepo(connection);
                EndDbCreating(connection, dbFile);
            }
            catch (StopProgramException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException("Возникла ошибка при создании БД", e);
            }
            GC.Collect();
        }

        public override FoundCommits FindCommit(IProcessedPatch targetPatch)
        {
            try
            {
                lock (longLifeConnection)
                {

                    var res = DeepCompare
                        ? FindCommitsBySubjectDeepCompare(targetPatch.Subject)
                        : FindCommitsBySubject(targetPatch.Subject);
                    return new FoundCommits(FindCommitsByDate(targetPatch.Date).ToList(), res.ToList());
                }
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException("Возникла ошибка при загрузке из БД", e);
            }
        }

        protected override IEnumerable<Commit> FindCommitsBySubjectDeepCompare(string targetMessage)
        {
            var temp = targetMessage.Split(' ');
            var msg1 = string.Join(' ', temp[(temp.Length / 2)..]);
            var msg2 = $"subject LIKE '%{msg1}%'";
            var idArr = SelectSubjectDeepCompare(longLifeConnection!, msg2);
            if (idArr == null)
                yield break;
            
            foreach (var commit in idArr)
                yield return RepoLookup(commit)!;
        }

        public void Dispose()
        {
            longLifeConnection.Dispose();
        }
    }
}