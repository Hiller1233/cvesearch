﻿using System;
using System.Collections.Generic;
using System.Linq;
using CveSearchMain.CommitSearch.PatchParser;
using CveSearchMain.Logger;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch.RepositoryWorker
{
    public abstract class RepositoryWorkerBase : IRepositoryWorker
    {
        protected const int MinPercentage = 60;
        
        protected readonly Repository Repo;
        protected readonly bool DeepCompare;

        protected RepositoryWorkerBase(Repository repo, bool deepCompare)
        {
            Repo = repo;
            DeepCompare = deepCompare;
        }

        public CommitSearchResult SearchCommit(ILogger logger, IProcessedPatch targetPatch)
        {
            logger.Log("Поиск коммита " + targetPatch.CommitId, LogSeverity.Debug);
            var commitFromLookup = RepoLookup(targetPatch.CommitId);
            if (commitFromLookup != null)
                return new CommitSearchResult(Status.FoundId, commitFromLookup.Id, (1, 1), 
                    targetPatch.CommitId, targetPatch.PatchLink);

            var result = FindCommit(targetPatch);
            var foundCommit = CheckCommitsAndTakeOne(logger, result.DateCommits, targetPatch, Status.FoundDate);
            if (foundCommit != null)
                return foundCommit;

            foundCommit = CheckCommitsAndTakeOne(logger, result.MessageCommits, targetPatch, Status.FoundMessage);
            if (foundCommit != null)
                return foundCommit;
            
            return new CommitSearchResult(Status.NotFound, null, (0, 0), 
                targetPatch.CommitId, targetPatch.PatchLink);
        }

        public abstract FoundCommits FindCommit(IProcessedPatch targetPatch);

        protected Commit? RepoLookup(ObjectId id) => Repo.Lookup<Commit>(id);

        protected abstract IEnumerable<Commit> FindCommitsBySubjectDeepCompare(string targetMessage);

        private CommitSearchResult? CheckCommitsAndTakeOne(ILogger logger, IEnumerable<Commit> commits,
            IProcessedPatch targetPatch, Status status)
        {
            CommitSearchResult? max = null;
            double maxAcc = 0;
            
            foreach (var res in CheckCommits(logger, commits, targetPatch, status))
            {
                if (res.Accuracy < MinPercentage)
                {
                    logger.Log($"Коммит {res.CommitId} пропущен, так как совпадает менее чем на {MinPercentage}%:" +
                                      $" {res.Accuracy:F2}%", LogSeverity.Debug);
                    continue;
                }

                if (res.Accuracy <= maxAcc)
                    continue;
                
                max = res;
                maxAcc = res.Accuracy;
            }

            return max;
        }

        private IEnumerable<CommitSearchResult> CheckCommits(ILogger logger, IEnumerable<Commit> commits,
            IProcessedPatch targetPatch, Status status)
        {
            foreach (var commit in commits)
            {
                logger.Debug("Проверка коммита " + commit.Id);
                Commit parent;
                try
                {
                    parent = commit.Parents.Single();
                }
                catch (Exception)
                {
                    logger.Debug("Коммит пропущен, так как не имеет ровно одного родителя");
                    continue;
                }
                var diff = Repo.Diff.Compare<Patch>(parent.Tree, commit.Tree);
                var patch = new LibGitPatch(commit, diff);
                var (p, ap) = DiffComparator.CompareDiffs(logger, patch, targetPatch, Repo.Info.Path);
                var tempRes = new CommitSearchResult(status, commit.Id, (ap, p), 
                    targetPatch.CommitId, targetPatch.PatchLink);
                logger.Debug($"Итого для коммита: {tempRes.Info}");
                yield return tempRes;
                if (ap == p)
                    yield break;
            }
        }
    }
}