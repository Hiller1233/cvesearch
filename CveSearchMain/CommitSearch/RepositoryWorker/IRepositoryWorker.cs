﻿using CveSearchMain.CommitSearch.PatchParser;
using CveSearchMain.Logger;

namespace CveSearchMain.CommitSearch.RepositoryWorker
{
    public interface IRepositoryWorker
    {
        CommitSearchResult SearchCommit(ILogger logger, IProcessedPatch targetPatch);

        FoundCommits FindCommit(IProcessedPatch targetPatch);
    }
}