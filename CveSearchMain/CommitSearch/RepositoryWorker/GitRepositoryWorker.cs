﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CveSearchMain.CommitSearch.PatchParser;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch.RepositoryWorker
{
    public class GitRepositoryWorker : RepositoryWorkerBase
    {
        public GitRepositoryWorker(Repository repo,  bool deepCompare) : base(repo, deepCompare)
        { }
        
        public override FoundCommits FindCommit(IProcessedPatch targetPatch)
        {
            var thisDateCommits = Repo.Commits
                .Where(c => c.Author.When.Equals(targetPatch.Date))
                .ToList();

            var res = DeepCompare
                ? FindCommitsBySubjectDeepCompare(targetPatch.Subject)
                : Repo.Commits.Where(c => c.MessageShort == targetPatch.Message);
            return new FoundCommits(thisDateCommits, res);
        }

        protected override IEnumerable<Commit> FindCommitsBySubjectDeepCompare(string targetMessage) 
            => Repo.Commits.Where(c => Regex.IsMatch(c.MessageShort, targetMessage));
    }
}