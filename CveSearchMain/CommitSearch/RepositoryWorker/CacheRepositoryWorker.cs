﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CveSearchMain.CommitSearch.PatchParser;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch.RepositoryWorker
{
    public class CacheRepositoryWorker : RepositoryWorkerBase
    {
        private readonly RepositoryCache.Collection cache;

        public CacheRepositoryWorker(Repository repo, bool deepCompare, 
            RepositoryCache.Collection cache) : base(repo, deepCompare)
        {
            this.cache = cache;
        }

        public override FoundCommits FindCommit(IProcessedPatch targetPatch)
        {
            var res = DeepCompare
                ? FindCommitsBySubjectDeepCompare(targetPatch.Subject)
                : FindCommitsBySubject(targetPatch.Subject);
            return new FoundCommits(FindCommitsByDate(targetPatch.Date).ToList(), res);
        }

        private IEnumerable<Commit> FindCommitsBySubject(string targetMessage)
        {
            var count = cache.GetIdBySubject(targetMessage, out var id, out var ids);
            if (count == 0)
                yield break;
            if (count > 1)
                foreach (var i in ids!)
                    yield return RepoLookup(new ObjectId(i))!;
            else
                yield return RepoLookup(new ObjectId(id))!;
        }
        
        private IEnumerable<Commit> FindCommitsByDate(DateTimeOffset targetDate)
        {
            var count = cache.GetIdByDate(targetDate, out var id, out var ids);
            if (count == 0)
                yield break;
            if (count > 1)
                foreach (var i in ids!)
                    yield return RepoLookup(new ObjectId(i))!;
            else
                yield return RepoLookup(new ObjectId(id))!;
        }

        protected override IEnumerable<Commit> FindCommitsBySubjectDeepCompare(string targetMessage) =>
            cache.GetAllSubjects()
                .Where(s => Regex.IsMatch(s, targetMessage))
                .SelectMany(subject => FindCommitsBySubject(targetMessage));
    }
}