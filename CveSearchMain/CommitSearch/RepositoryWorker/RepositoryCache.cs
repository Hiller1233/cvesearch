﻿using System;
using System.Collections.Generic;
using System.IO;
using CveSearchMain.Logger;
using LibGit2Sharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CveSearchMain.CommitSearch.RepositoryWorker
{
    public static class RepositoryCache
    {
        public static Collection BuildRepositoryCache(ILogger logger, Repository repo, string dir, bool saveResults,
            bool loadFromFile)
        {
            logger.Log("Обработка репозитория для создания кэша");
            var repoId = repo.Head.Tip.Sha;
            var file = new FileInfo(Path.Join(dir, $"cache-{repoId}.json"));
            Collection? col = null;
            if (loadFromFile)
            {
                col = BuildCollectionFromFile(logger, file);
                if (col != null)
                    return col;
            }

            logger.Log("Создание кэша репозитория");
            if (saveResults)
                col = BuildAndSaveCollection(logger, repo, file);
            
            col ??= BuildCollectionWithoutSave(repo);
            return col;
        }
        
        private static Collection? BuildCollectionFromFile(ILogger logger, FileInfo file)
        {
            if (!file.Exists)
            {
                logger.Log("Файл кэша репозитория не найден");
                return null;
            }
            var dictMessage = new Dictionary<string, string>();
            var dictMessageExtra = new Dictionary<string, List<string>>();
            var dictDate = new Dictionary<DateTimeOffset, string>();
            var dictDateExtra = new Dictionary<DateTimeOffset, List<string>>();

            try
            {
                using var reader = file.OpenText();
                using var jReader = new JsonTextReader(reader);
                logger.Log("Считывание из файла кэша", LogSeverity.Debug);
                try
                {
                    jReader.Read();
                    while (true)
                    {
                        jReader.Read();
                        if (jReader.TokenType == JsonToken.EndArray)
                            break;
                        
                        var token = JToken.ReadFrom(jReader).ToObject<CommitContainer>()!;
                        FillDict(dictMessage, dictMessageExtra, token.Subject, token.Id);
                        FillDict(dictDate, dictDateExtra, token.Date, token.Id);
                    }
                }
                catch (Exception)
                {
                    throw logger.LogAndCreateStopProgramException("Ошибка при чтении данных", LogSeverity.Error);
                }
            }
            catch (StopProgramException)
            {
                return null;
            }
            catch (Exception)
            {
                logger.Log("Произошла ошибка при чтении из файла " + file.FullName, LogSeverity.Error);
                return null;
            }
            
            logger.Log("Кэш загружен из файла " + file.FullName, LogSeverity.Success);
            return new Collection(dictMessage, dictMessageExtra, dictDate, dictDateExtra);
        }

        private static Collection BuildCollectionWithoutSave(Repository repo)
        {
            var dictMessage = new Dictionary<string, string>();
            var dictMessageExtra = new Dictionary<string, List<string>>();
            var dictDate = new Dictionary<DateTimeOffset, string>();
            var dictDateExtra = new Dictionary<DateTimeOffset, List<string>>();
            foreach (var c in repo.Commits)
            {
                FillDict(dictMessage, dictMessageExtra, c.MessageShort, c.Sha);
                FillDict(dictDate, dictDateExtra, c.Author.When, c.Sha);
            }
            return new Collection(dictMessage, dictMessageExtra, dictDate, dictDateExtra);
        }
        
        private static Collection? BuildAndSaveCollection(ILogger logger, Repository repo, FileInfo file)
        {
            var dictMessage = new Dictionary<string, string>();
            var dictMessageExtra = new Dictionary<string, List<string>>();
            var dictDate = new Dictionary<DateTimeOffset, string>();
            var dictDateExtra = new Dictionary<DateTimeOffset, List<string>>();
            try
            {
                if (file.Directory != null && !file.Directory.Exists)
                    file.Directory.Create();
            
                using var writer = file.OpenWrite();
                using var w = new StreamWriter(writer);
                using var jWriter = new JsonTextWriter(w);
                logger.Log("Начало записи в файл кэша", LogSeverity.Debug);
                try
                {
                    var ser = new JsonSerializer {Formatting = Formatting.Indented};
                    jWriter.WriteStartArray();
                    foreach (var c in repo.Commits)
                    {
                        FillDict(dictMessage, dictMessageExtra, c.MessageShort, c.Sha);
                        FillDict(dictDate, dictDateExtra, c.Author.When, c.Sha);
                        ser.Serialize(jWriter, new CommitContainer(c.Sha, c.MessageShort, c.Author.When));
                    }
                    jWriter.WriteEndArray();
                }
                catch (Exception)
                {
                    throw logger.LogAndCreateStopProgramException("Ошибка при сериализации данных");
                }
            }
            catch (StopProgramException)
            {
                return null;
            }
            catch (Exception)
            {
                logger.Log("Произошла ошибка при записи в файл " + file.FullName, LogSeverity.Error);
                return null;
            }
            
            logger.Log("Кэш сохранен в файл " + file.FullName, LogSeverity.Success);
            return new Collection(dictMessage, dictMessageExtra, dictDate, dictDateExtra);
        }

        private static void FillDict<T1, T2>(Dictionary<T1, T2> dict, Dictionary<T1, List<T2>> ex, T1 key, T2 val)
            where T1 : notnull
        {
            if (!dict.ContainsKey(key))
            {
                if (ex.ContainsKey(key))
                    ex[key].Add(val);
                else
                    dict[key] = val;
                return;
            }

            var prev = dict[key];
            dict.Remove(key);
            ex[key] = new List<T2> {prev, val};
        }

        public class Collection
        {
            private readonly Dictionary<string, string> dictMessage;
            private readonly Dictionary<string, List<string>> dictMessageExtra;
            private readonly Dictionary<DateTimeOffset, string> dictDate;
            private readonly Dictionary<DateTimeOffset, List<string>> dictDateExtra;

            internal Collection(Dictionary<string, string> dictMessage, 
                Dictionary<string, List<string>> dictMessageExtra, 
                Dictionary<DateTimeOffset, string> dictDate, 
                Dictionary<DateTimeOffset, List<string>> dictDateExtra)
            {
                this.dictMessage = dictMessage;
                this.dictMessageExtra = dictMessageExtra;
                this.dictDate = dictDate;
                this.dictDateExtra = dictDateExtra;
            }

            public int GetIdBySubject(string subject, out string? single, out IReadOnlyList<string>? multiple)
            {
                single = null;
                multiple = null;
                if (dictMessage.ContainsKey(subject))
                {
                    single = dictMessage[subject];
                    return 1;
                }

                if (dictMessageExtra.ContainsKey(subject))
                {
                    multiple = dictMessageExtra[subject];
                    return multiple.Count;
                }

                return 0;
            }
            
            public int GetIdByDate(DateTimeOffset date, out string? single, out IReadOnlyList<string>? multiple)
            {
                single = null;
                multiple = null;
                if (dictDate.ContainsKey(date))
                {
                    single = dictDate[date];
                    return 1;
                }

                if (dictDateExtra.ContainsKey(date))
                {
                    multiple = dictDateExtra[date];
                    return multiple.Count;
                }

                return 0;
            }

            public IEnumerable<string> GetAllSubjects()
            {
                foreach (var key in dictMessage.Keys)
                    yield return key;
                foreach (var key in dictMessageExtra.Keys)
                    yield return key;
            }
            
        }

        private struct CommitContainer
        {
            public CommitContainer(string id, string subject, DateTimeOffset date)
            {
                Id = id;
                Subject = subject;
                Date = date;
            }

            public string Id { get; }
            public string Subject { get; }
            public DateTimeOffset Date { get; }
        }
    }
}