﻿using System.Collections.Generic;

namespace CveSearchMain.CommitSearch.PatchParser
{
    public struct DiffFileResult
    {
        private IReadOnlyDictionary<string, List<int>>? changesInDictionary;

        public DiffFileResult(IReadOnlyList<string> changes, string fileName, string plainDiff)
        {
            Changes = changes;
            FileName = fileName;
            PlainDiff = plainDiff;
            changesInDictionary = null;
        }

        public IReadOnlyList<string> Changes { get; }
        public string FileName { get; }
        public string PlainDiff { get; }
        public int CountLines => Changes.Count / 2;
        public IReadOnlyDictionary<string, List<int>> ChangesInDictionary 
            => changesInDictionary ??= TransformList(Changes);

        private static IReadOnlyDictionary<string, List<int>> TransformList(IReadOnlyList<string> changes)
        {
            var dict = new Dictionary<string, List<int>>();
            for (var i = 0; i < changes.Count; i += 2)
            {
                var line = changes[i];
                if (!dict.ContainsKey(line))
                    dict[line] = new List<int>();
                dict[line].Add(i);
            }

            return dict;
        }
    }
}