﻿using System.Collections.Generic;

namespace CveSearchMain.CommitSearch.PatchParser
{
    public interface IProcessedPatch : IRawPatch
    {
        DiffFileResult TakeDiff(string file);
        IEnumerable<DiffFileResult> ChangedFiles { get; }
        string PatchLink { get; }
    }
}