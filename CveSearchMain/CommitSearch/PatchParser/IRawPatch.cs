﻿using System;
using System.Collections.Generic;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch.PatchParser
{
    public interface IRawPatch
    {
        string Patch { get; }
        DateTimeOffset Date { get; }
        string Subject { get; }
        string Message { get; }
        int Count { get; }
        ObjectId CommitId { get; }

        public IEnumerable<string> FileNames { get; }
        IEnumerable<(string path, string patch)> ChangedFilesOnlyDiff { get; }
        bool Contains(string file);
    }
}