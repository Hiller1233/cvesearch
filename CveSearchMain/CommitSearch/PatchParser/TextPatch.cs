﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch.PatchParser
{
    public class TextPatch : IEnumerable<DiffFileResult>, IProcessedPatch
    {
        private readonly Dictionary<string, DiffFileResult> changes;
        private IEnumerable<string>? fileNames;

        public TextPatch(IEnumerable<DiffFileResult> changedFiles, string patch, DateTimeOffset date, string subject,
            string message, ObjectId commitId, string patchLink)
        {
            changes = changedFiles.ToDictionary(c => c.FileName, c => c);
            Patch = patch;
            Date = date;
            Subject = subject;
            Message = message;
            CommitId = commitId;
            PatchLink = patchLink;
        }

        public DiffFileResult TakeDiff(string path) => changes[path]; // Нет смысла в проверке

        public bool Contains(string path) => changes.ContainsKey(path);

        
        public string Patch { get; }
        public DateTimeOffset Date { get; }
        public string Subject { get; }
        public string Message { get; }
        public int Count => changes.Count;
        public ObjectId CommitId { get; }

        public IEnumerable<string> FileNames => fileNames ??= changes.Values.Select(c => c.FileName);

        public IEnumerator<DiffFileResult> GetEnumerator() => ChangedFiles.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerable<(string path, string patch)> ChangedFilesOnlyDiff
            => ChangedFiles.Select(d => (d.FileName, d.PlainDiff));
        public IEnumerable<DiffFileResult> ChangedFiles => changes.Values;
        public string PatchLink { get; }
    }
}