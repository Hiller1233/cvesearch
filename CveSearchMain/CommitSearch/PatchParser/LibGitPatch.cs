﻿using System;
using System.Collections.Generic;
using System.Linq;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch.PatchParser
{
    public class LibGitPatch : IRawPatch
    {
        private readonly Commit libGitCommit;
        private readonly Patch libGitPatch;

        public LibGitPatch(Commit libGitCommit, Patch libGitPatch)
        {
            this.libGitCommit = libGitCommit;
            this.libGitPatch = libGitPatch;
        }

        public string Patch => libGitPatch.Content;
        public DateTimeOffset Date => libGitCommit.Author.When;
        public string Subject => libGitCommit.MessageShort;
        public string Message => libGitCommit.Message;
        public int Count => libGitPatch.LinesAdded + libGitPatch.LinesDeleted;
        public ObjectId CommitId => libGitCommit.Id;
        public IEnumerable<string> FileNames => libGitPatch.Select(c => c.Path);

        public IEnumerable<(string path, string patch)> ChangedFilesOnlyDiff =>
            libGitPatch.Select(c => (c.Path, c.Patch));
        public bool Contains(string file) => libGitPatch[file] != null;
    }
}