﻿using System;
using System.Collections.Generic;
using System.Text;
using CveSearchMain.Logger;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch.PatchParser
{
    public static class Parser
    {
        public static TextPatch ParsePatch(ILogger logger, string patch, string link)
        {
            var sb = new StringBuilder();
            var date = DateTimeOffset.MinValue;
            ObjectId commitId = null!;
            var subject = string.Empty;
            var message = string.Empty;
            var flag = 0;
            var split = patch.Split('\n');
            var list = new List<DiffFileResult>();

            for (var i = 0; i < split.Length; i++)
            {
                if (split[i].StartsWith("From "))
                {
                    if ((flag & 8) == 8)
                        throw logger.LogAndCreateStopProgramException("Патч имеет дубликат id");
                    if (!ObjectId.TryParse(split[i][5..45], out commitId))
                        throw logger.LogAndCreateStopProgramException("Не удалось получить id");
                    flag |= 8;
                    i++;
                }
                
                if (split[i].StartsWith("Date: "))
                {
                    if ((flag & 1) == 1)
                        throw logger.LogAndCreateStopProgramException("Патч имеет дубликат даты");
                    if (!DateTimeOffset.TryParse(split[i][6..], out date))
                        throw logger.LogAndCreateStopProgramException("Не удалось получить дату");
                    flag |= 1;
                    i++;
                }

                if (split[i].StartsWith("Subject: "))
                {
                    if ((flag & 2) == 2)
                        throw logger.LogAndCreateStopProgramException("Патч имеет дубликат короткого описания");
                    sb.Clear().Append(GetFirstLineOfSubject(logger, split[i][9..]));
                    for (++i; i < split.Length; i++)
                    {
                        if (split[i] == "")
                        {
                            flag |= 2;
                            subject = sb.ToString();
                            break;
                        }

                        sb.Append(split[i]);
                    }
                    
                    sb.Clear();
                    for (++i; i < split.Length; i++)
                    {
                        if (split[i] == "---")
                        {
                            flag |= 4;
                            message = sb.ToString().TrimEnd('\n');
                            break;
                        }

                        sb.AppendLine(split[i]);
                    }
                }
                
                if (flag != 15)
                    continue;

                if (!split[i].StartsWith("diff"))
                    continue;
                
                list.Add(ParseFile(logger, split[i..], out var offset));
                i += offset - 1;
            }
            
            if (flag != 15)
                throw logger.LogAndCreateStopProgramException("Патч имеет имеет неверный формат");
                
            logger.Log("Патч успешно загружен", LogSeverity.Success);
            return new TextPatch(list, patch, date, subject, message == "" ? subject : message, commitId, link);
        }

        private static string GetFirstLineOfSubject(ILogger logger, string s)
        {
            if (!s.StartsWith("[PATCH"))
                return s;
            if (s.StartsWith("[PATCH] "))
                return s[8..];
            throw logger.LogAndCreateStopProgramException(
                "Неожиданный заголовок diff в патче, скорее всего коммит имеет тип merge");
        }

        private static DiffFileResult ParseFile(ILogger logger, ReadOnlySpan<string> file, out int offset)
        {
            var fileName = string.Empty;
            for (offset = 0; offset < file.Length; offset++)
            {
                if (!file[offset].StartsWith("--- "))
                    continue;

                fileName = file[offset++][6..];
                var plus = file[offset++][6..];
                if (plus != "ev/null")
                    fileName = plus;
                break;
            }

            if (string.IsNullOrEmpty(fileName))
                throw logger.LogAndCreateStopProgramException("Неожиданный заголовок diff в патче");
            
            var list = new List<string>();
            for (offset = 5; offset < file.Length; offset++)
            {
                var line = file[offset];

                if (line.StartsWith("diff"))
                    break;

                if ((line.StartsWith('+') || line.StartsWith('-')) && !line.StartsWith("++") && !line.StartsWith("--"))
                {
                    list.Add(line);
                    var prevLine = file[offset - 1];
                    list.Add(prevLine.StartsWith('@') ? string.Empty : prevLine);
                }
            }
            
            var sb = new StringBuilder();
            foreach (var line in file[..(offset - 1)])
                sb.Append(line).Append('\n');

            return new DiffFileResult(list, fileName, sb.ToString(0, sb.Length - 1));
        }
    }
}