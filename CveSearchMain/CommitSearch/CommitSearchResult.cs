﻿using System;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch
{
    public class CommitSearchResult
    {
        private string? message;

        public CommitSearchResult(Status status, ObjectId? commitId, (int actual, int max) points, 
            ObjectId? cveCommitId, string cveLink)
        {
            Status = status;
            CommitId = commitId;
            Points = points;
            CveCommitId = cveCommitId;
            CveLink = cveLink;
            Accuracy = Points.max == 0 ? 0 : (double) Points.actual / Points.max * 100;
            Info = $"{Points.actual}/{Points.max} ({Accuracy:F2}%)";
        }

        public Status Status { get; }

        public string Message =>
            message ??= Status switch
            {
                Status.NotFound => "Искомый коммит не обнаружен",
                Status.Error => "Возникла ошибка при получении патча",
                Status.FoundId => $"Обнаружен коммит {CommitId}",
                Status.FoundDate => $"Обнаружен коммит с той же датой {CommitId}. Соответствие: {Info}",
                Status.FoundMessage =>
                $"Обнаружен коммит с похожим заголовком {CommitId}. Соответствие: {Info}",
                _ => throw new ArgumentOutOfRangeException()
            };

        public ObjectId? CommitId { get; }
        public ObjectId? CveCommitId { get; }
        public (int actual, int max) Points { get; }
        public double Accuracy { get; }
        public string Info { get; }
        public string CveLink { get; }
        
        public string? DebugMessage { get; set; }
    }
}