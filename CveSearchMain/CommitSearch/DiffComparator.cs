﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CveSearchMain.CommitSearch.PatchParser;
using CveSearchMain.Logger;

namespace CveSearchMain.CommitSearch
{
    public static class DiffComparator
    {
        private const int oneFilePoint = 3;
        private const int missingFilePoint = 50;
        private const int oneLinePoint = 3;
        private const int prevLinePoint = 1;
        
        public static (int, int) CompareDiffs(ILogger logger, IRawPatch patch, IProcessedPatch targetPatch, 
            string pathToRepo)
        {
            var actualFiles = new HashSet<string>();
            var badPath = false;
            var actualPoints = 0;
            var points = 0;
            foreach (var (file, diff) in patch.ChangedFilesOnlyDiff)
            {
                points += oneFilePoint;
                if (!targetPatch.Contains(file))
                {
                    logger.Debug($"Патч содержит лишний файл {file}");
                    points += missingFilePoint;
                    badPath = true;
                    continue;
                }

                actualFiles.Add(file);
                actualPoints += oneFilePoint;
                var targetDiff = targetPatch.TakeDiff(file);
                if (targetDiff.PlainDiff == diff)
                {
                    var delta = targetDiff.Changes.Count / 2 * (oneLinePoint + prevLinePoint);
                    points += delta;
                    actualPoints += delta;
                    logger.Debug($"Изменения в файле {file} полностью совпадают ({delta})");
                    continue;
                }
                
                logger.Debug($"Проверка изменений в файле {file}");
                var (p, ap) = CompareTextsFast(logger, diff, targetDiff);
                logger.Debug($"Итого: {ap}/{p}");
                actualPoints += ap;
                points += p;
            }

            if (!badPath && actualFiles.Count == targetPatch.Count)
                return (points, actualPoints);
            
            foreach (var path in targetPatch.FileNames
                .Where(p => !actualFiles.Contains(p)))
            {
                points += oneFilePoint;
                try
                {
                    var file = new FileInfo(Path.Join(pathToRepo, path));
                    if (file.Exists)
                        logger.Debug($"Проверяемый патч не содержит файла {path}");
                    else
                    {
                        actualPoints += oneFilePoint;
                        logger.Debug($"Патч не содержит файла {path}, но этого файла нет и в репозитории");
                    }
                }
                catch (Exception)
                {
                    logger.Log($"Произошла ошибка при проверке существования файла {path}", LogSeverity.Error);
                }
            }

            return (points, actualPoints);
        }

        private static (int, int) CompareTextsFast(ILogger logger, string text, DiffFileResult diffFileResult)
        {
            var dict = new Dictionary<string, int>();
            var actualPoints = 0;
            var points = 0;
            var count = 0;
            var diffLines = text.Split('\n');
            var prev = string.Empty;
            
            foreach (var line in diffLines)
            {
                if (!line.StartsWith('+') && !line.StartsWith('-') || line.StartsWith("++") || line.StartsWith("--"))
                {
                    prev = line;
                    continue;
                }

                points += oneLinePoint;
                if (!diffFileResult.ChangesInDictionary.ContainsKey(line))
                {
                    logger.Debug($"Несовпадающая строка: @{line}@");
                    prev = line;
                    continue;
                }

                if (!dict.ContainsKey(line))
                    dict[line] = 0;
                dict[line] += 1;

                count++;
                actualPoints += oneLinePoint;
                points += prevLinePoint;
                var firstPrev = diffFileResult.ChangesInDictionary[line]
                    .FirstOrDefault(l => diffFileResult.Changes[l + 1] == prev);
                if (firstPrev != 0 || diffFileResult.Changes[1] == prev || prev.StartsWith("@@"))
                    actualPoints += prevLinePoint;
                prev = line;
            }

            points += (diffFileResult.CountLines - count) * oneLinePoint;
            return (points, actualPoints);
        }
    }
}