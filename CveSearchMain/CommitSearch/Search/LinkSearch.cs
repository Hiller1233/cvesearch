﻿using System;
using System.Collections.Generic;
using CveSearchMain.CommitSearch.RepositoryWorker;
using CveSearchMain.Logger;

namespace CveSearchMain.CommitSearch.Search
{
    public class LinkSearch : SearchBase
    {
        private readonly ILogger logger;
        private readonly IRepositoryWorker repoWorker;
        private readonly IEnumerable<string> links;
        private readonly List<CommitSearchResult> listToFill;
        private readonly bool throwOnError;

        public LinkSearch(ILogger logger, IRepositoryWorker repoWorker, IEnumerable<string> links, 
            List<CommitSearchResult> listToFill, bool throwOnError)
        {
            this.logger = logger;
            this.repoWorker = repoWorker;
            this.links = links;
            this.listToFill = listToFill;
            this.throwOnError = throwOnError;
        }

        public override void Start()
        {
            foreach (var link in links)
            {
                try
                {
                    var res = InternalSearch(logger, repoWorker, link, throwOnError);
                    logger.Log(res.Message);
                    listToFill.Add(res);
                }
                catch (StopProgramException)
                {
                    if (throwOnError)
                        throw;
                }
                catch (Exception e)
                {
                    var msg = "Ошибка при поиске коммита по ссылке " + link;
                    if (throwOnError)
                        throw logger.LogAndCreateStopProgramException(msg, e);
                    logger.Log(msg);
                }
            }
            
            if (listToFill.Count == 0)
                logger.Log("Не удалось найти ни одного результата");
        }
    }
}