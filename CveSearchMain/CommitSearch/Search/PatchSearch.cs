﻿using System;
using System.Collections.Generic;
using System.Linq;
using CveSearchMain.CommitSearch.RepositoryWorker;
using CveSearchMain.CveAnalyzer;
using CveSearchMain.Logger;

namespace CveSearchMain.CommitSearch.Search
{
    public class PatchSearch : SearchBase
    {
        protected readonly ILogger Logger;
        protected readonly IRepositoryWorker RepoWorker;
        protected readonly IReadOnlyList<CveAnalyzerResult> AnResults;
        private readonly bool throwOnError;

        public PatchSearch(ILogger logger, IRepositoryWorker repoWorker, IReadOnlyList<CveAnalyzerResult> anResults, 
            bool throwOnError)
        {
            Logger = logger;
            RepoWorker = repoWorker;
            AnResults = anResults;
            this.throwOnError = throwOnError;
        }

        public override void Start()
        {
            for (var i = 0; i < AnResults.Count; i++)
            {
                var res = AnResults[i];
                Logger.Log($"Поиск коммитов для {res.Cve.Id} [{i + 1}/{AnResults.Count}]");
                SearchCommitsForCve(Logger, res, RepoWorker, throwOnError);
            }
        }

        protected static void SearchCommitsForCve(ILogger logger, CveAnalyzerResult res, IRepositoryWorker repoWorker, 
            bool throwOnError)
        {
            try
            {
                if (res.Cve.PatchRefs.Count == 0)
                {
                    logger.Log("Не обнаружено патчей для поиска");
                    return;
                }

                res.Commits = res.Cve.PatchRefs
                    .Select(l =>
                    {
                        var innerLogger = new PatchInfoLogger(logger);
                        var r = InternalSearch(innerLogger, repoWorker, l, throwOnError);
                        innerLogger.Log(r.Message);
                        r.DebugMessage = innerLogger.GetContent();
                        return r;
                    })
                    .ToList();
            }
            catch (StopProgramException)
            {
                if (throwOnError)
                    throw;
            }
            catch (Exception e)
            {
                var msg = "** Ошибка при поиске коммита для " + res.Cve.Id;
                if (throwOnError)
                    throw logger.LogAndCreateStopProgramException(msg, e);
                logger.Log(msg);
            }
        }
    }
}