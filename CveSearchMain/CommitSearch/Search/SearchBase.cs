﻿using System;
using CveSearchMain.CommitSearch.PatchParser;
using CveSearchMain.CommitSearch.RepositoryWorker;
using CveSearchMain.Logger;
using CveSearchMain.NvdDownloader;

namespace CveSearchMain.CommitSearch.Search
{
    public abstract class SearchBase : ISearch
    {
        public abstract void Start();
        
        protected static CommitSearchResult InternalSearch(ILogger logger, IRepositoryWorker repoWorker, 
            string linkToCommit, bool throwOnError)
        {
            logger.Log($"Обработка патча {linkToCommit}");
            try
            {
                return InternalSearchBase(logger, repoWorker, linkToCommit);
            }
            catch (Exception)
            {
                if (throwOnError)
                    throw;
                return new CommitSearchResult(Status.Error, null, (0, 0), null, linkToCommit);
            }
        }
        
        private static CommitSearchResult InternalSearchBase(ILogger logger, IRepositoryWorker repoWorker, 
        string linkToCommit)
        {
            string rawPatch;
            try
            {
                rawPatch = NvdDownloaderMain.DownloadDataFromLinkCustom(logger, linkToCommit);
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException("Не удалось загрузить патч", e);
            }

            var patch = Parser.ParsePatch(logger, rawPatch, linkToCommit);
            return repoWorker.SearchCommit(logger, patch);
        }
    }
}