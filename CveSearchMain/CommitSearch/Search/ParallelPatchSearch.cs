﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CveSearchMain.CommitSearch.RepositoryWorker;
using CveSearchMain.CveAnalyzer;
using CveSearchMain.Logger;

namespace CveSearchMain.CommitSearch.Search
{
    public class ParallelPatchSearch : PatchSearch
    {
        public ParallelPatchSearch(ILogger logger, IRepositoryWorker repoWorker, 
            IReadOnlyList<CveAnalyzerResult> anResults) : base(logger, repoWorker, anResults, false)
        { }

        public override void Start()
        {
            var count = 0;
            Parallel.For(0, AnResults.Count, i =>
            {
                var res = AnResults[i];
                var innerLogger = new AccumulativeConsoleLogger(Logger);
                SearchCommitsForCve(innerLogger, res, RepoWorker, false);
                var it = Interlocked.Increment(ref count);
                innerLogger.Flush($"Поиск коммитов для {res.Cve.Id} [{it}/{AnResults.Count}]");
            });
        }
    }
}