﻿using System.Collections.Generic;
using LibGit2Sharp;

namespace CveSearchMain.CommitSearch
{
    public class FoundCommits
    {
        public FoundCommits(IReadOnlyList<Commit> dateCommits, IEnumerable<Commit> messageCommits)
        {
            DateCommits = dateCommits;
            MessageCommits = messageCommits;
        }
        
        public IReadOnlyList<Commit> DateCommits { get; }
        public IEnumerable<Commit> MessageCommits { get; }
    }
}