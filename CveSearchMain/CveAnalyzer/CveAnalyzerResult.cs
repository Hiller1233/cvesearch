﻿using System.Collections.Generic;
using CveSearchMain.CommitSearch;
using CveSearchMain.CveParser;

namespace CveSearchMain.CveAnalyzer
{
    public class CveAnalyzerResult
    {
        public CveAnalyzerResult(Cve cve)
        {
            Cve = cve;
        }

        public Cve Cve { get; }
        public IReadOnlyList<CommitSearchResult>? Commits { get; set; }
    }
}