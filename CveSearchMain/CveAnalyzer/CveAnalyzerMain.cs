﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CveSearchMain.CommitSearch;
using CveSearchMain.CveAnalyzer.Report;
using CveSearchMain.CveParser;
using CveSearchMain.InputParser;
using CveSearchMain.Logger;

namespace CveSearchMain.CveAnalyzer
{
    public class CveAnalyzerMain : IModule
    {
        private readonly ILogger logger;
        private ArgsParser<Args>? parser;
        
        private static string DefaultNdvFolder => @"nvd";
        private static string DefaultTempFolder => @"temp";
        private static string DefaultFormat => @"nvdcve-1.1-{0}.json";
        private static string DefaultCveSource => "pdb";
        private static int DefaultResultsPerPage => 100;

        public CveAnalyzerMain(ILogger logger)
        {
            this.logger = logger;
        }

        public void AnalyzeCve(Args args)
        {
            ValidateAndFixArgs(args);
            var cveParser = new CveParserMain(logger);
            var parserArgs = new CveParserMain.Args
            {
                CveArray = args.AnalyzerEntries,
                DoNotPrintResultsToConsole = true,
                DoNotLoadCveFromFile = args.DoNotLoadFromFile,
                DoNotSaveCveToFile = args.DoNotSaveToFile,
                FileNameFormat = args.FileNameFormat,
                NvdFolder = args.NvdFolder,
                TempFolder = args.TempFolder,
                StopProgramOnError = args.StopProgramOnError,
                Keywords = args.Keywords,
                ResultsPerPage = args.ResultsPerPage,
                DoNotLoadDb = args.DoNotLoadCveDb,
                CveSource = args.CveSource,
                StartDate = args.StartDate,
                EndDate = args.EndDate
            };
            var cveArr = cveParser.ReceiveVulnerabilities(parserArgs)
                .Select(cve => new CveAnalyzerResult(cve))
                .ToList();
            if (cveArr.Count == 0)
            {
                logger.Log("Не обнаружено CVE для анализа");
                return;
            }
            var commitSearch = new CommitSearchMain(logger);
            var searchArgs = new CommitSearchMain.Args
            {
                DoNotPrintResultsToConsole = true,
                PathToRepo = args.PathToRepo,
                StopProgramOnError = args.StopProgramOnError,
                AnalyzerEntries = cveArr,
                DeepCompare = args.DeepCompare,
                DoNotLoadCacheFromFile = args.DoNotLoadCacheFromFile,
                DoNotSaveCacheToFile = args.DoNotSaveCacheToFile,
                TempFolder = args.TempFolder,
                LinksToCommit = null,
                Keywords = args.Keywords,
                UseRequestCache = !args.DoNotUseRequestCache,
                LoadRequests = !args.DoNotLoadRequests,
                SaveRequests = !args.DoNotSaveRequests,
                RepositoryWorker = args.RepositoryWorker,
                DoNotUseParallelSearch = args.DoNotUseParallelSearch
            };
            var _ = commitSearch.StartCommitSearch(searchArgs);
            if (args.PrintResultsToConsole)
                PrintToConsole(cveArr);
            
            if (!ReportManager.CreateReport(logger, cveArr, new FileInfo(args.PathToRepo).FullName, args.Keywords, 
                args.StartDate, args.EndDate))
                PrintToFile(cveArr);
        }
        
        private static void ValidateAndFixArgs(Args args)
        {
            if (args.Keywords != null && args.Keywords.Length == 0)
                args.Keywords = null;
            if (args.AnalyzerEntries != null && args.AnalyzerEntries.Length == 0)
                args.AnalyzerEntries = null;
            if (args.Keywords != null)
                args.AnalyzerEntries = null;
            else if (args.AnalyzerEntries == null)
                throw new StopProgramException("** Для получения CVE нужно их перечислить или указать слова для поиска");
            if (args.Keywords == null && (!args.DoNotLoadRequests || !args.DoNotSaveRequests))
            {
                Console.WriteLine("При поиске уязвимостей из списка сохранение/загрузка запросов не поддерживается");
                args.DoNotLoadRequests = true;
                args.DoNotSaveRequests = true;
            }
            if (args.PathToRepo == null)
                throw new StopProgramException("** Путь к репозиторию должен быть задан явно");
            if (args.FileNameFormat != null)
                try
                {
                    var _ = string.Format(args.FileNameFormat, "2020");
                }
                catch (Exception)
                {
                    throw new StopProgramException("** Задан неверный формат имени файла");
                }
            args.NvdFolder ??= DefaultNdvFolder;
            args.TempFolder ??= DefaultTempFolder;
            args.FileNameFormat ??= DefaultFormat;
            try
            {
                var _ = string.Format(args.FileNameFormat, "2020");
            }
            catch (Exception)
            {
                throw new StopProgramException("** Задан неверный формат имени файла");
            }
            
            args.ResultsPerPage ??= DefaultResultsPerPage;
            if (args.ResultsPerPage > 5000 || args.ResultsPerPage < 10)
                throw new StopProgramException("** Значение получаемых CVE должно быть 10-5000");

            if (args.DoNotUseRequestCache)
            {
                args.DoNotLoadRequests = true;
                args.DoNotSaveRequests = true;
            }
            
            args.CveSource ??= DefaultCveSource;
            if (args.CveSource != "file" && args.CveSource != "req" 
                                         && args.CveSource != "db" && args.CveSource != "pdb")
                throw new StopProgramException("** Источник cve имеет неверное значение");
        }

        private string MakePrintString(CveAnalyzerResult el, StringBuilder sb)
        {
            sb.AppendLine($"= {el.Cve.Id} === {el.Cve.Impact} = {el.Cve.LastModifiedDate} = {el.Cve.PublishedDate}")
                .AppendLine(el.Cve.Description).AppendLine();
            foreach (var cwe in el.Cve.CweCollection)
                sb.AppendLine(cwe);
            foreach (var refer in el.Cve.References)
                sb.AppendLine(refer);
            sb.AppendLine("Результаты по поиску патча:");
            if (el.Commits == null || el.Commits.Count == 0)
                sb.AppendLine("Не удалось обнаружить ссылки на патчи");
            else
                for (var i = 0; i < el.Commits.Count; i++)
                {
                    var res = el.Commits[i];
                    sb.AppendLine($"Патч {i + 1}/{el.Commits.Count}");
                    sb.AppendLine(res == null ? "** Произошла ошибка" : res.Message);
                }

            var msg = sb.ToString();
            sb.Clear();
            return msg;
        }

        private void PrintToConsole(IEnumerable<CveAnalyzerResult> arr)
        {
            var sb = new StringBuilder();
            foreach (var el in arr) 
                Console.WriteLine(MakePrintString(el, sb));
        }
        
        private void PrintToFile(IEnumerable<CveAnalyzerResult> arr)
        {
            logger.Log("Попытка записи результатов в файл");
            var file = new FileInfo($"result-{DateTime.Now.Ticks}.txt");
            var sb = new StringBuilder();
            try
            {
                using var stream = file.OpenWrite();
                using var writer = new StreamWriter(stream);
                foreach (var el in arr) 
                    writer.WriteLine(MakePrintString(el, sb));
            }
            catch (Exception)
            {
                logger.Log("Не удалось совершить запись в файл", LogSeverity.Error);
            }
            
            logger.Log($"Результаты сохранены в текстовый файл {file.FullName}", LogSeverity.Success);
        }

        public void ProcessArgs(string[] args)
        {
            parser ??= new ArgsParser<Args>();
            var parsedArgs = parser.ParseArgs(args);
            if (parsedArgs != null)
                AnalyzeCve(parsedArgs);
        }

        public class Args : IInputArgs
        {
            #region Common

            [CommandInfo("--print", "-pr", "выводить результат в консоль")] 
            public bool PrintResultsToConsole { get; set; } 
            
            [CommandInfo("--stop-on-error", "-s", "при ошибке при обработке cve программа остановится")] 
            public bool StopProgramOnError { get; set; }
            
            [CommandInfo("--cve-list", "-c", "искомые CVE (аргумент вводится последним)")]
            public string[]? AnalyzerEntries { get; set; }
            
            [CommandInfo("--keywords", "-k", "задает ключевые слова для поиска (должен быть указан последним)")]
            public string[]? Keywords { get; set; }
            
            [CommandInfo("--temp-dir", "-td", "директория, в которой следует сохранять файлы")]
            public string? TempFolder { get; set; }
            
            [CommandInfo("--do-not-save", "-ns", "не сохранять данные во временные файлы")]
            public bool DoNotSaveToFile { get; set; }
            
            [CommandInfo("--do-not-load", "-nl", "не загружать данные из временных файлов")]
            public bool DoNotLoadFromFile { get; set; }

            #endregion

            #region CommitSearch

            [CommandInfo("--path", "-p", "путь к репозиторию git")]
            public string? PathToRepo { get; set; }
            
            [CommandInfo("--deep-compare", "-dp", "использовать полный и медленный поиск по теме")]
            public bool DeepCompare  { get; set; }

            [CommandInfo("--do-not-save-cache", "-ncs", "не сохранять кэш репозитория в файл (не применяется к БД)")]
            public bool DoNotSaveCacheToFile  { get; set; }
            
            [CommandInfo("--do-not-load-cache", "-ncl", "не загружать кэш/БД репозитория из файла")]
            public bool DoNotLoadCacheFromFile  { get; set; }
            
            [CommandInfo("--do-not-save-requests", "-nsr", "не сохранять запросы в файл")]
            public bool DoNotSaveRequests  { get; set; }
            
            [CommandInfo("--do-not-load-requests", "-nlr", "не загружать запросы из файла")]
            public bool DoNotLoadRequests  { get; set; }
            
            [CommandInfo("--do-not-use-request-cache", "-nrc", "не использовать кэш запросов")]
            public bool DoNotUseRequestCache  { get; set; }
            
            [CommandInfo("--repository-worker", "-rw", "обработчик репозитория ([db], cache, file)")]
            public string? RepositoryWorker { get; set; }
            
            [CommandInfo("--do-not-use-parallel-search", "-nps", "не использовать параллельный поиск коммитов")]
            public bool DoNotUseParallelSearch  { get; set; }

            #endregion
            
            #region CveParser
            
            [CommandInfo("--nvd-dir", "-dn", "директория, в которой следует искать БД NVD")]
            public string? NvdFolder { get; set; }
            
            [CommandInfo("--format", "-f", "формат имени файла БД NVD")]
            public string? FileNameFormat { get; set; }
            
            [CommandInfo("--results-per-page", "-rpp", "устанавливает желаемое число CVE на один запрос")]
            public int? ResultsPerPage { get; set; }

            [CommandInfo("--date-start", "-ds", "устанавливает начальную дату поиска")]
            public DateTime? StartDate { get; set; }
            
            [CommandInfo("--date-end", "-de", "устанавливает конечную дату поиска")]
            public DateTime? EndDate { get; set; }
            
            [CommandInfo("--do-not-load-db", "-nldb", "не загружать готовую базу данных cve")]
            public bool DoNotLoadCveDb { get; set; }
            
            [CommandInfo("--cve-source", "-cs", "источник cve ([pdb], db, file, req)")]
            public string? CveSource { get; set; }

            #endregion
        }
    }
}