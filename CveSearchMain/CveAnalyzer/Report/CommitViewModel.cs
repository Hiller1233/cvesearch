﻿namespace CveSearchMain.CveAnalyzer.Report
{
    public class CommitViewModel
    {
        public string Number { get; set; } = null!;
        public string CvePatchId { get; set; } = null!;
        public string RepoPatchId { get; set; } = null!;
        public string Ref { get; set; } = null!;
        public string ResultMessage { get; set; } = null!;
        public string Color { get; set; } = null!;
        public string Percentages { get; set; } = null!;
        public string Score { get; set; } = null!;
        public string ScoreBoxHeight { get; set; } = null!;
        public string DebugMessage { get; set; } = null!;
        
        public bool HavePatch { get; set; }
        public bool HaveCvePatch { get; set; }
        public bool HaveDebug { get; set; }
    }
}