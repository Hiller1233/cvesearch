﻿using System;
using System.Collections.Generic;
using System.Linq;
using CveSearchMain.CommitSearch;

namespace CveSearchMain.CveAnalyzer.Report
{
    public partial class Report
    {
        private readonly List<CveViewModel> cveCollection;
        private readonly Dictionary<string, ColorEntry> colorStats;
        private readonly string pathToRepo;
        private readonly DateTimeOffset? startDate;
        private readonly DateTimeOffset? endDate;
        private readonly string typeString;
        private readonly string time;

        public Report(List<CveAnalyzerResult> cveArr, string pathToRepo, string[]? keywords, DateTimeOffset? startDate, 
            DateTimeOffset? endDate)
        {
            this.pathToRepo = pathToRepo;
            this.startDate = startDate;
            this.endDate = endDate;
            if (keywords != null)
                typeString = "Поиск по ключевым словам:" + string.Join(' ', keywords);
            else
                typeString = "Поиск по уязвимостям: " + string.Join(' ', cveArr.Select(cve => cve.Cve.Id));
            var now = DateTime.Now;
            time = $"{now.ToShortTimeString()} {now.ToShortDateString()}";
            cveCollection = new List<CveViewModel>();
            colorStats = new Dictionary<string, ColorEntry>();
            for (var i = 0; i < cveArr.Count; i++)
            {
                var cve = BuildCveViewModel(cveArr[i]);
                cve.Number = (i + 1).ToString();
                for (var j = 0; j < cve.Commits.Count; j++) 
                    cve.Commits[j].Number = (j + 1).ToString();
                cveCollection.Add(cve);
                if (!colorStats.ContainsKey(cve.ResultColor))
                    colorStats[cve.ResultColor] = new ColorEntry(cve.ResultColor);
                
                colorStats[cve.ResultColor].Count++;
            }
            
            foreach (var entry in colorStats.Values) 
                entry.ComputePx(cveCollection.Count);
            colorStats = colorStats
                .OrderByDescending(p => p.Value.Count)
                .ToDictionary(p => p.Key, p => p.Value);
        }

        private static CveViewModel BuildCveViewModel(CveAnalyzerResult res)
        {
            var (resultMessage, resultColor) = Analyze(res);
            var commits = res.Commits == null || res.Commits.Count == 0
                ? new List<CommitViewModel>()
                : res.Commits.Select(BuildCommitViewModel).ToList();
            return new CveViewModel
            {
                ResultColor = resultColor,
                ResultMessage = resultMessage,
                CveId = res.Cve.Id,
                CveDescription = res.Cve.Description,
                References = res.Cve.References,
                PatchRefs = res.Cve.PatchRefs,
                Commits = commits
            };
        }

        private static (string resultMessage, string resultColor) Analyze(CveAnalyzerResult res)
        {
            if (res.Cve.PatchRefs.Count == 0)
                return ("Не обнаружено патчей для анализа", "black");
            if (res.Commits == null || res.Commits.Count == 0)
                return ("Не обнаружено ни одного патча", "red");
            var failedPatches = res.Commits.Count(c => c.Status == Status.NotFound || c.Status == Status.Error) 
                                + (res.Cve.PatchRefs.Count - res.Commits.Count);
            if (failedPatches == res.Cve.PatchRefs.Count)
                return ("Не обнаружено ни одного патча", "red");
            if (failedPatches != 0)
                return ("Не удалось обнаружить часть патчей", "yellow");
            var minPer = res.Commits.Min(c => c.Accuracy);
            if (minPer < 60)
                return ("Все патчи найдены, но минимальный процент совпадения слишком низкий", "red");
            if (minPer < 85)
                return ("Все патчи найдены, но минимальный процент совпадения не достаточно высок", "yellow");
            if (100 - minPer < 0.0001)
                return ("Все патчи найдены в репозитории", "blue");
            return ("Все патчи найдены в репозитории, но есть незначительные различия", "green");
        }

        private static CommitViewModel BuildCommitViewModel(CommitSearchResult res)
        {
            var color = 100 - res.Accuracy < 0.0001
                    ? "blue"
                    : res.Accuracy < 60
                        ? "red"
                        : res.Accuracy < 85
                            ? "yellow"
                            : "green";
            var havePatch = res.Status != Status.NotFound && res.Status != Status.Error;
            var haveCvePatch = res.Status != Status.Error;

            var scoreBoxHeight = ((int)(70 * (1 - res.Accuracy / 100))).ToString();
            return new CommitViewModel
            {
                DebugMessage = res.DebugMessage ?? "",
                Color = color,
                CvePatchId = haveCvePatch ? res.CveCommitId?.Sha ?? "" : "",
                RepoPatchId = havePatch ? res.CommitId?.Sha ?? "" : "",
                Score = $"{res.Points.actual}/{res.Points.max}",
                ScoreBoxHeight = scoreBoxHeight,
                Percentages = $"{res.Accuracy:F2}%",
                Ref = res.CveLink,
                ResultMessage = res.Message,
                HaveDebug = res.DebugMessage != null,
                HavePatch = havePatch,
                HaveCvePatch = haveCvePatch
            };
        }

        private class ColorEntry
        {
            public ColorEntry(string color)
            {
                Color = color;
            }

            public string Color { get; }
            public int Count { get; set; }
            public int ColorBoxPx { get; set; }
            public int WhiteBoxPx { get; set; }

            public void ComputePx(int allCount)
            {
                ColorBoxPx = (int) Math.Round(Count / (double) allCount * 100);
                if (ColorBoxPx == 0)
                    ColorBoxPx = 1;

                WhiteBoxPx = 100 - ColorBoxPx;
            }

            public string Title
            {
                get
                {
                    return Color switch
                    {
                        "red" => "Уязвимости, для которых не удалось найти патч или его процент совпадения слишком " +
                                 "низкий",
                        "green" => "Уязвимости, для которых найдены все патчи, но есть небольшие неточности",
                        "yellow" => "Уязвимости, для которых найдены не все патчи или процент совпадения не " +
                                    "достаточно высок",
                        "black" => "Уязвимости, при анализе которых не удалось найти ни одного патча",
                        "blue" => "Уязвимости, для которых найдены все патчи",
                        _ => throw new ArgumentException()
                    };
                }
            }
        }
    }
}