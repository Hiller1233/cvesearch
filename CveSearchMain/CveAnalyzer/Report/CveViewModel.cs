﻿using System.Collections.Generic;

namespace CveSearchMain.CveAnalyzer.Report
{
    public class CveViewModel
    {
        public string ResultMessage { get; set; } = null!;
        public string ResultColor { get; set; } = null!;
        public string CveId { get; set; } = null!;
        public string CveDescription { get; set; } = null!;
        public string Number { get; set; } = null!;
        
        public IReadOnlyList<string> References { get; set; } = null!;
        public IReadOnlyList<string> PatchRefs { get; set; } = null!;
        public List<CommitViewModel> Commits { get; set; } = null!;
    }
}