﻿using System;
using System.Collections.Generic;
using System.IO;
using CveSearchMain.Logger;

namespace CveSearchMain.CveAnalyzer.Report
{
    public static class ReportManager
    {
        public static bool CreateReport(ILogger logger, List<CveAnalyzerResult> cveArr, string pathToRepo, 
            string[]? keywords, DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            logger.Log("Создание отчета");
            string content;
            try
            {
                var report = new Report(cveArr, pathToRepo, keywords, startDate, endDate);
                content = report.TransformText();
            }
            catch (Exception)
            {
                logger.Log("Не удалось создать отчет", LogSeverity.Error);
                return false;
            }

            var file = new FileInfo($"report-{DateTime.Now.Ticks}.html");
            try
            {
                File.WriteAllText(file.FullName, content);
            }
            catch (Exception)
            {
                logger.Log("Не удалось сохранить отчет", LogSeverity.Error);
                return false;
            }

            logger.Log($"Отчет сохранен в файл {file.FullName}", LogSeverity.Success);
            return true;
        }
    }
}