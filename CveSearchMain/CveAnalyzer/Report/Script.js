function onExpanderClick(elem, target) {
    elem.classList.toggle("expanded");
    let exp = document.getElementById(target);
    exp.classList.toggle("expanded");
    if (exp.style.maxHeight) {
        exp.style.maxHeight = null;
    } else {
        exp.style.maxHeight = exp.scrollHeight + "px";
    }
}

function onFButtonClick(elem, classTarget, numId, num){
    if (elem.classList.contains("active"))
        document.getElementById(numId).innerHTML = "0/"+num;
    else
        document.getElementById(numId).innerHTML = num+"/"+num;
    elem.classList.toggle("active");
    let elems = document.getElementsByClassName(classTarget);
    for (let i = 0; i < elems.length; i++)
        elems[i].classList.toggle("no-display");
}