﻿using System;

namespace CveSearchMain
{
    public static class Program
    {
        public static bool IsDebug { get; private set; }

        public static int Main(string[] args)
        {
#if DEBUG
            IsDebug = true;
#endif
            try
            {
                var dispatcher = new MainDispatcher();
                dispatcher.Start(args);
            }
            catch (StopProgramException e)
            {
                return HandleStopProgramException(e);
            }
            catch (Exception e)
            {
                if (e.InnerException != null && e.InnerException is StopProgramException st)
                    return HandleStopProgramException(st);
                Console.WriteLine(e.Message);
                Console.WriteLine("** Работа программы завершилась с неизвестной ошибкой");
                if (IsDebug)
                {
                    Console.WriteLine(e.StackTrace ?? "Трассировка недоступна");
                    Console.WriteLine("Нажмите любую клавишу для выхода из программы...");
                    Console.ReadKey();
                }

                return 1;
            }

            Console.WriteLine("Программа завершена.");
            if (IsDebug)
            {
                Console.WriteLine("Нажмите любую клавишу для выхода из программы...");
                Console.ReadKey();
            }

            return 0;
        }

        public static void DefineDebug()
        {
            IsDebug = true;
        }

        public static int HandleStopProgramException(StopProgramException e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine("** Работа программы завершилась с ошибками");
            if (IsDebug)
            {
                Console.WriteLine("Нажмите любую клавишу для выхода из программы...");
                Console.ReadKey();
            }

            return 1;
        }
    }
}