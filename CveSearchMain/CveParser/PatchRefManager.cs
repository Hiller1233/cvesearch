﻿using System;
using System.Collections.Generic;
using LibGit2Sharp;

namespace CveSearchMain.CveParser
{
    public static class PatchRefManager
    {
        public static IReadOnlyList<string> HandlePatches(IReadOnlyList<string> refs)
        {
            var res = new List<string>();
            var state = State.Empty;
            foreach (var r in refs)
            {
                var (level, p) = ProcessRef(r);
                if (p == null)
                    continue;
                
                if ((int) level > (int) state)
                {
                    state = level;
                    res.Clear();
                    res.Add(p);
                    continue;
                }
                
                if (level == state && !res.Contains(p))
                    res.Add(p);
            }

            return res;
        }

        private static (State level, string? patch) ProcessRef(string r)
        {
            if (r.Contains("github.com", StringComparison.OrdinalIgnoreCase))
                return (State.GitHub, ProcessGitHub(r));
            
            if (r.Contains("kernel.org", StringComparison.OrdinalIgnoreCase))
                return (State.KernelGit, ProcessGitKernel(r));
            
            if (r.Contains("sourceware.org", StringComparison.OrdinalIgnoreCase))
                return (State.SourceWare, ProcessSourceWare(r));
            
            return (State.Empty, null);
        }

        private static string? ProcessGitHub(string r)
        {
            return r.Length > 50 && ObjectId.TryParse(r[^40..], out _) ? r + ".patch" : null;
        }

        private static string? ProcessGitKernel(string r)
        {
            return r.Length > 50 && ObjectId.TryParse(r[^40..], out var id) 
                ? $"https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/patch/?id={id}" 
                : null;
        }

        private static string? ProcessSourceWare(string r)
        {
            return r.Length > 50 && ObjectId.TryParse(r[^40..], out _) 
                ? r.Replace("commitdiff", "patch").Replace("commit", "patch") + ";a=patch"
                : null;
        }

        private enum State
        {
            Empty = 0,
            GitHub,
            KernelGit,
            SourceWare
        }
    }
}