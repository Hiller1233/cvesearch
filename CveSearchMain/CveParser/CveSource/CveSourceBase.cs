﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CveSearchMain.CveParser.CveSource
{
    public abstract class CveSourceBase : ICveSource
    {
        protected static void ReadFromStream(StreamReader reader,
            Action<JsonTextReader> checkHeaders, Action<IEnumerable<JToken>> action)
        {
            try
            {
                using var jsonReader = new JsonTextReader(reader);

                checkHeaders(jsonReader);
                var jsonArr = EnumerateJsonArray(jsonReader);
                action(jsonArr);
            }
            catch (StopProgramException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new StopProgramException($"** Ошибка при парсинге JSON", e);
            }
        }
        
        protected static void ReadCveFileFromStream(StreamReader reader, Action<IEnumerable<JToken>> action)
        {
            ReadFromStream(reader, r =>
            {
                if (!ValidateJsonHeader(r)) 
                    throw new StopProgramException("** Неожиданный формат JSON");
            },
                action);
        }
        
        protected static (int, int) ReadCveResponseFromStream(StreamReader reader, Action<IEnumerable<JToken>> action)
        {
            int? startIndex = 0;
            int? total = 0;
            ReadFromStream(reader, r =>
                {
                    bool ok;
                    (ok, startIndex, total) = ValidateResponseFirstJsonHeader(r);
                    if (!ok)
                        throw new StopProgramException("** Неожиданный формат ответа NVD");
                    if (!ValidateResponseSecondJsonHeader(r)) 
                        throw new StopProgramException("** Неожиданный формат JSON");
                },
                action);


            return (startIndex ?? 0, total ?? 0);
        }

        protected static IEnumerable<JToken> EnumerateJsonArray(JsonTextReader reader)
        {
            while (true)
            {
                reader.Read();
                if (reader.TokenType == JsonToken.EndArray)
                    yield break;
                
                yield return JToken.ReadFrom(reader);
            }
        }

        protected static bool ValidateJsonHeader(JsonTextReader reader)
        {
            var arr = new List<(JsonToken token, string? value)>
            {
                (JsonToken.StartObject, null),
                (JsonToken.PropertyName, "CVE_data_type"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_data_format"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_data_version"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_data_numberOfCVEs"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_data_timestamp"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_Items"),
                (JsonToken.StartArray, null)
            };
            
            return ValidateJsonHeaderBase(reader, arr);
        }

        [SuppressMessage("ReSharper", "ParameterOnlyUsedForPreconditionCheck.Local")]
        protected static (bool ok, int startIndex, int total) ValidateResponseFirstJsonHeader(JsonTextReader reader)
        {
            static void Ensure(bool pr)
            {
                if (!pr)
                    throw new ArgumentException();
            }
            
            try
            {
                reader.Read();
                Ensure(reader.TokenType == JsonToken.StartObject);
                
                reader.Read();
                Ensure(reader.TokenType == JsonToken.PropertyName);
                Ensure((string) reader.Value! == "resultsPerPage");
                reader.Read();
                
                reader.Read();
                Ensure(reader.TokenType == JsonToken.PropertyName);
                Ensure((string) reader.Value! == "startIndex");
                var st = (int) reader.ReadAsInt32()!;

                reader.Read();
                Ensure(reader.TokenType == JsonToken.PropertyName);
                Ensure((string) reader.Value! == "totalResults");
                var tot = (int) reader.ReadAsInt32()!;
                
                reader.Read();
                Ensure(reader.TokenType == JsonToken.PropertyName);
                Ensure((string) reader.Value! == "result");

                return (true, st, tot);
            }
            catch (Exception)
            {
                return (false, 0, 0);
            }
        }

        protected static bool ValidateResponseSecondJsonHeader(JsonTextReader reader)
        {
            var arr = new List<(JsonToken token, string? value)>
            {
                (JsonToken.StartObject, null),
                (JsonToken.PropertyName, "CVE_data_type"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_data_format"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_data_version"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_data_timestamp"),
                (JsonToken.String, null),
                (JsonToken.PropertyName, "CVE_Items"),
                (JsonToken.StartArray, null)
            };
            
            return ValidateJsonHeaderBase(reader, arr);
        }

        protected static IEnumerable<Cve> ParseJsonCve(IEnumerable<JToken> tokens, bool throwOnError)
        {
            foreach (var token in tokens)
            {
                Cve cve;
                try
                {
                    cve = token.ToObject<JsonCve>()!.ToCve();
                }
                catch (Exception)
                {
                    var id = (string) token["cve"]?["CVE_data_meta"]?["ID"]!;
                    if (string.IsNullOrEmpty(id))
                        id = "CVE";
                    var msg = "** Ошибка при парсинге: " + id;
                    if (throwOnError)
                        throw new StopProgramException(msg);
                    Console.WriteLine(msg);
                    continue;
                }
                yield return cve;
            }
        }

        private static bool ValidateJsonHeaderBase(JsonTextReader reader, List<(JsonToken token, string? value)> arr)
        {
            var it = 0;
            while (true)
            {
                reader.Read();
                if (reader.TokenType != arr[it].token)
                    return false;
                if (reader.Value == null)
                {
                    if (arr[it].value != null)
                        return false;
                }
                else
                {
                    if (arr[it].value != reader.Value.ToString())
                        return false;
                }
                
                it++;
                if (reader.TokenType == JsonToken.StartObject)
                    continue;

                reader.Read();
                if (reader.TokenType == JsonToken.PropertyName)
                    return false;
                if (reader.TokenType == JsonToken.StartArray)
                {
                    if (arr[it].token == JsonToken.StartArray)
                        break;
                    return false;
                }
                
                it++;
                if (it >= arr.Count)
                    return false;
            }

            return true;
        }

        public abstract IReadOnlyList<Cve> GetVulnerabilitiesFromList(HashSet<string> cveArr, bool throwOnError);

        public abstract IReadOnlyList<Cve> GetVulnerabilitiesFromKeywords(string[] keywords, bool throwOnError,
            DateTime? beginDate, DateTime? endDate);
    }
}