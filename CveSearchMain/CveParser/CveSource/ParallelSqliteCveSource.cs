﻿using System.IO;
using System.Threading.Tasks;
using CveSearchMain.Logger;
using Microsoft.Data.Sqlite;

namespace CveSearchMain.CveParser.CveSource
{
    public class ParallelSqliteCveSource : SqliteCveSource
    {
        public ParallelSqliteCveSource(ILogger logger, string nvdFileFormat, string nvdFolder, string tempFolder,
            bool loadFromFile) 
            : base(logger, nvdFileFormat, nvdFolder, tempFolder, loadFromFile)
        { }

        protected override void StartExecution(SqliteConnection connection, FileInfo[] files, bool throwOnError)
        {
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 4 }, file =>
            {
                using var parallelConnection = new SqliteConnection(connection.ConnectionString);
                parallelConnection.Open();
                FillDbFromFile(parallelConnection, file, throwOnError);
            });
        }
    }
}