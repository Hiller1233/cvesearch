﻿using System;
using System.Collections.Generic;

namespace CveSearchMain.CveParser.CveSource
{
    public interface ICveSource
    {
        public IReadOnlyList<Cve> GetVulnerabilitiesFromList(HashSet<string> cveArr, bool throwOnError);
        public IReadOnlyList<Cve> GetVulnerabilitiesFromKeywords(string[] keywords, bool throwOnError, DateTime? beginDate, DateTime? endDate);
    }
}