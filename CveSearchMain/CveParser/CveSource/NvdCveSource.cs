﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CveSearchMain.NvdDownloader;

namespace CveSearchMain.CveParser.CveSource
{
    public class NvdCveSource : CveSourceBase
    {
        private readonly int resultsPerPage;


        public NvdCveSource(int resultsPerPage)
        {
            this.resultsPerPage = resultsPerPage;
        }

        private async Task<(int startIndex, int total, List<Cve>)> DownloadPageFromNvd(string keyString, int startIndex,
            DateTime? beginDate, DateTime? endDate, bool throwOnError)
        {
            var client = NvdDownloaderMain.Client;
            var link = CreateLink(keyString, startIndex, resultsPerPage, beginDate, endDate);
            try
            {
                using var res = await client.GetAsync(link, HttpCompletionOption.ResponseHeadersRead);
                if (!res.IsSuccessStatusCode)
                    throw new StopProgramException("** При загрузке NVD вернул ошибку с кодом " +
                                                   $"{(int)res.StatusCode} ({res.StatusCode})");
                await using var stream = await res.Content.ReadAsStreamAsync();
                using var reader = new StreamReader(stream);
                List<Cve> list = null!;
                var (st, total) = ReadCveResponseFromStream(reader, en =>
                    {
                        list = ParseJsonCve(en, throwOnError).ToList();
                    });
                return (st, total, list);
            }
            catch (StopProgramException)
            {
                throw;
            }
            catch (Exception)
            {
                Console.WriteLine($"** Возникла ошибка при загрузке данных: {link}");
                throw;
            }
        }

        private static string CreateLink(string keyString, int startIndex, int resultsPerPage,
            DateTimeOffset? modStartDate, DateTimeOffset? modEndDate)
        {
            var link = "https://services.nvd.nist.gov/rest/json/cves/1.0" +
                       $"?keyword={keyString}&startIndex={startIndex}&resultsPerPage={resultsPerPage}";
            var format = "yyyy-MM-dd'T'HH:mm:ss':000 UTC-05:00'";
            if (modStartDate != null)
                link += $"&modStartDate={modStartDate.Value.ToUniversalTime().ToString(format)}";
            if (modEndDate != null)
                link += $"&modEndDate={modEndDate.Value.ToUniversalTime().ToString(format)}";

            return link;
        }

        public override IReadOnlyList<Cve> GetVulnerabilitiesFromList(HashSet<string> cveArr, bool throwOnError)
        {
            throw new StopProgramException("** Получение CVE через БД NVD не поддерживается");
        }

        public override IReadOnlyList<Cve> GetVulnerabilitiesFromKeywords(string[] keywords, bool throwOnError,
            DateTime? beginDate, DateTime? endDate)
        {
            Console.WriteLine("Получение CVE из NVD с использованием ключевых слов");
            var keyString = string.Join('+', keywords);
            var (_, total, list) = DownloadPageFromNvd(keyString, 0, beginDate, endDate, throwOnError).Result;
            var it = resultsPerPage;
            var res = list;
            while (it < total)
            {
                Console.WriteLine($"Получено {res.Count}/{total}");
                Task.Delay(1000).Wait();
                // Задержка перед запросом
                // При установке количества страниц на 1000 (чтение с 1000 по 2000) соединение разрывается
                // Тоже самое происходит и в браузере
                // https://services.nvd.nist.gov/rest/json/cves/1.0?keyword=kernel+linux&startIndex=1000&resultsPerPage=1000
                (_, _, list) = DownloadPageFromNvd(keyString, it, beginDate, endDate, throwOnError).Result;
                it += resultsPerPage;
                res.AddRange(list);
            }
            Console.WriteLine($"Загрузка {res.Count}/{total} CVE завершена");
            return res;
        }
    }
}