﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CveSearchMain.CveParser.CveSource
{
    public class FileCveSource : CveSourceBase
    {
        private readonly string nvdFolder;
        private readonly string fileNameFormat;

        public FileCveSource(string nvdFolder, string fileNameFormat)
        {
            this.nvdFolder = nvdFolder;
            this.fileNameFormat = fileNameFormat;
        }
        
        private IReadOnlyDictionary<string, Cve> SearchVulnerabilitiesInFile(FileInfo file, HashSet<string> cveArr, 
            bool throwOnError)
        {
            IReadOnlyDictionary<string, Cve> dict;
            try
            {
                Console.WriteLine($"Чтение из файла {file.Name}");
                dict = SearchVulnerabilitiesInFileInternal(file, cveArr, throwOnError);
            }
            catch (StopProgramException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new StopProgramException($"** Ошибки при чтении из файла {file.FullName}");
            }
            
            return dict;
        }

        private IReadOnlyDictionary<string, Cve> SearchVulnerabilitiesInFileInternal(FileInfo file, 
            HashSet<string> cveArr, bool throwOnError)
        {
            using var fileReader = file.OpenText();

            Dictionary<string, Cve> dict = null!;
            ReadCveFileFromStream(fileReader, en =>
            {
                var temp = en
                    .Where(jToken => cveArr.Contains((string) jToken["cve"]?["CVE_data_meta"]?["ID"]!))
                    .Take(cveArr.Count);
                dict = ParseJsonCve(temp, throwOnError)
                    .ToDictionary(cve => cve.Id, cve => cve);
            });

            if (cveArr.Count != dict.Count)
                foreach (var s in cveArr.Where(cve => !dict.ContainsKey(cve)))
                {
                    var msg = $"** Не удалось найти уязвимость {s} в файле {file.Name}";
                    if (throwOnError)
                        throw new StopProgramException(msg);
                    Console.WriteLine(msg);
                }
            else
            {
                Console.WriteLine($"Все уязвимости в файле {file.Name} найдены");
            }

            return dict;
        }

        private Dictionary<string, HashSet<string>> TransformCveArray(HashSet<string> cveArr)
        {
            var dict = new Dictionary<string, HashSet<string>>();
            foreach (var cve in cveArr)
            {
                var year = cve[4..8];
                if (!dict.ContainsKey(year)) 
                    dict[year] = new HashSet<string>();
                dict[year].Add(cve);
            }
            return dict;
        }
        
        public override IReadOnlyList<Cve> GetVulnerabilitiesFromList(HashSet<string> cveArr, bool throwOnError)
        {
            Console.WriteLine("Получение CVE из файлов БД NVD");
            if (cveArr == null || cveArr.Count == 0)
            {
                Console.WriteLine("Не обнаружено CVE для поиска");
                return new List<Cve>();
            }
            
            var cveDict = TransformCveArray(cveArr);
            var res = new List<Cve>();
            foreach (var (year, set) in cveDict)
            {
                var path = Path.Join(nvdFolder, string.Format(fileNameFormat, year));
                var file = new FileInfo(path);
                var dict = SearchVulnerabilitiesInFile(file, set, throwOnError);
                res.AddRange(dict.Values);
            }

            return res;
        }

        public override IReadOnlyList<Cve> GetVulnerabilitiesFromKeywords(string[] keywords, bool throwOnError,
            DateTime? beginDate, DateTime? endDate)
        {
            throw new StopProgramException("** Поиск в файлах БД NVD по ключевым словам не поддерживается");
        }
    }
}