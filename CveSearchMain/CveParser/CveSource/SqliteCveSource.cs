﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using CveSearchMain.Logger;
using Microsoft.Data.Sqlite;

namespace CveSearchMain.CveParser.CveSource
{
    public class SqliteCveSource : CveSourceBase
    {
        private readonly ILogger logger;
        private readonly string nvdFileFormat;
        private readonly string nvdFolder;
        private readonly string tempFolder;
        private readonly bool loadFromFile;

        public SqliteCveSource(ILogger logger, string nvdFileFormat, string nvdFolder, string tempFolder,
            bool loadFromFile)
        {
            this.logger = logger;
            this.loadFromFile = loadFromFile;
            this.nvdFileFormat = nvdFileFormat;
            this.nvdFolder = nvdFolder;
            this.tempFolder = tempFolder;
        }
        
        private static void CreateTables(SqliteConnection connection)
        {
            const string text = @"
CREATE TABLE vulnerabilities
(
    id        integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    cve       varchar(20) UNIQUE                NOT NULL,
    desc      varchar(20)                       NOT NULL,
    impact    varchar(10)                       NOT NULL,
    cvss2     varchar(10)                       NOT NULL,
    cvss3     varchar(10)                       NOT NULL,
    published TEXT                              NOT NULL,
    modified  TEXT                              NOT NULL
);

CREATE TABLE refs
(
    id  integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    ref TEXT UNIQUE                       NOT NULL
);

CREATE TABLE cwes
(
    id  integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    cwe varchar(20) UNIQUE                NOT NULL
);

CREATE TABLE cwe_for_cve
(
    cve_id integer NOT NULL,
    cwe_id integer NOT NULL,
    CONSTRAINT pk_cwe_for_cve PRIMARY KEY (cve_id, cwe_id),
    FOREIGN KEY (cve_id) REFERENCES vulnerabilities (id),
    FOREIGN KEY (cwe_id) REFERENCES cwes (id)
);

CREATE TABLE refs_for_cve
(
    cve_id integer NOT NULL,
    ref_id integer NOT NULL,
    CONSTRAINT pk_cwe_for_cve PRIMARY KEY (cve_id, ref_id),
    FOREIGN KEY (cve_id) REFERENCES vulnerabilities (id),
    FOREIGN KEY (ref_id) REFERENCES refs (id)
);

CREATE TABLE patches_for_cve
(
    cve_id integer NOT NULL,
    ref_id integer NOT NULL,
    CONSTRAINT pk_cwe_for_cve PRIMARY KEY (cve_id, ref_id),
    FOREIGN KEY (cve_id) REFERENCES vulnerabilities (id),
    FOREIGN KEY (ref_id) REFERENCES refs (id)
);
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = text;
            cmd.ExecuteNonQuery();
        }
        
        private static void AddCve(SqliteConnection connection, Cve cve)
        {
            const string text = @"
INSERT INTO vulnerabilities (cve, desc, impact, cvss2, cvss3, published, modified)
VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}');
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = String.Format(text, cve.Id, cve.Description.Replace("\'", "\'\'"), cve.Impact, cve.CvssV2, cve.CvssV3,
                cve.PublishedDate.ToString("yyyy-MM-dd"), cve.LastModifiedDate.ToString("yyyy-MM-dd"));
            cmd.ExecuteNonQuery();
            foreach (var c in cve.CweCollection)
                AddCwe(connection, cve.Id, c);
            foreach (var r in cve.References)
                AddReference(connection, cve.Id, r.Replace("\'", "\'\'"));
            foreach (var p in cve.PatchRefs)
                AddPatch(connection, cve.Id, p.Replace("\'", "\'\'"));
        }

        private static void AddReference(SqliteConnection connection, string cveId, string r)
        {
            const string text = @"
INSERT OR IGNORE INTO refs (ref) VALUES ('{0}');
INSERT INTO refs_for_cve (cve_id, ref_id)
VALUES ((SELECT id FROM vulnerabilities WHERE cve = '{1}'), (SELECT id FROM refs WHERE ref = '{0}'));
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = string.Format(text, r, cveId);
            cmd.ExecuteNonQuery();
        }

        private static void AddPatch(SqliteConnection connection, string cveId, string p)
        {
            const string text = @"
INSERT OR IGNORE INTO refs (ref) VALUES ('{0}');
INSERT INTO patches_for_cve (cve_id, ref_id)
VALUES ((SELECT id FROM vulnerabilities WHERE cve = '{1}'), (SELECT id FROM refs WHERE ref = '{0}'));
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = string.Format(text, p, cveId);
            cmd.ExecuteNonQuery();
        }

        private static void AddCwe(SqliteConnection connection, string cveId, string cwe)
        {
            const string text = @"
INSERT OR IGNORE INTO cwes (cwe) VALUES ('{0}');
INSERT INTO main.cwe_for_cve (cve_id, cwe_id)
VALUES ((SELECT id FROM vulnerabilities WHERE cve = '{1}'), (SELECT id FROM cwes WHERE cwe = '{0}'));
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = string.Format(text, cwe, cveId);
            cmd.ExecuteNonQuery();
        }

        private static string AddCveArrayConstraint(HashSet<string> cveArr) 
            => string.Join(" AND ", cveArr.Select(c => $"cve = '{c}'"));

        private static string AddKeywordsConstraint(string[] keywords) 
            => string.Join(" AND ", keywords.Select(k => $"desc LIKE '%{k}%'"));

        private static string AddBeginDateConstraint(DateTime date) 
            => $" AND modified >= '{date:yyyy-MM-dd}'";
        
        private static string AddEndDateConstraint(DateTime date) 
            => $" AND modified <= '{date:yyyy-MM-dd}'";

        private IReadOnlyList<Cve> Select(SqliteConnection connection, string constraint, bool throwOnError)
        {
            const string text = @"
SELECT vulnerabilities.*, cwe, p.ref, r.ref
FROM main.vulnerabilities
         LEFT JOIN cwe_for_cve cfc ON vulnerabilities.id = cfc.cve_id
         LEFT JOIN cwes c on cfc.cwe_id = c.id
         LEFT JOIN patches_for_cve pfc on vulnerabilities.id = pfc.cve_id
         LEFT JOIN refs p on pfc.ref_id = p.id
         LEFT JOIN refs_for_cve rfc on vulnerabilities.id = rfc.cve_id
         LEFT JOIN refs r on rfc.ref_id = r.id
WHERE {0};
";
            using var cmd = connection.CreateCommand();
            cmd.CommandText = string.Format(text, constraint);
            using var reader = cmd.ExecuteReader();
            if (!reader.HasRows)
                return new List<Cve>();
            
            var cveDict = new Dictionary<string, Cve>();
            var cweDict = new Dictionary<string, List<string>>();
            var patchDict = new Dictionary<string, List<string>>();
            var refDict = new Dictionary<string, List<string>>();
            foreach (DbDataRecord? entry in reader)
            {
                try
                {
                    var id = entry!.GetString(1);
                    if (!cveDict.ContainsKey(id))
                        cveDict[id] = new Cve()
                        {
                            Id = entry.GetString(1),
                            Description = entry.GetString(2),
                            Impact = entry.GetString(3),
                            CvssV2 = entry.GetString(4),
                            CvssV3 = entry.GetString(5),
                            PublishedDate = DateTime.Parse(entry.GetString(6)),
                            LastModifiedDate = DateTime.Parse(entry.GetString(7))
                        };

                    TryAdd(cweDict, id, entry.IsDBNull(8) ? null : entry!.GetString(8));
                    TryAdd(patchDict, id, entry.IsDBNull(9) ? null : entry!.GetString(9));
                    TryAdd(refDict, id, entry.IsDBNull(10) ? null : entry!.GetString(10));
                }
                catch (Exception e)
                {
                    var msg = "Ошибка при чтении из БД";
                    if (throwOnError)
                        throw logger.LogAndCreateStopProgramException(msg, e);
                    logger.Log(msg, LogSeverity.Error);
                }
            }

            return cveDict
                .Select(pair =>
            {
                var (id, cve) = pair;
                cve.CweCollection = cweDict.ContainsKey(id) ? cweDict[id] : new List<string>();
                cve.PatchRefs = patchDict.ContainsKey(id) ? patchDict[id] : new List<string>();
                cve.References = refDict.ContainsKey(id) ? refDict[id] : new List<string>();
                return cve;
            })
                .ToList();
        }

        private static void TryAdd(Dictionary<string, List<string>> dict, string key, string? value)
        {
            if (value == null)
                return;
            
            if (!dict.ContainsKey(key))
                dict[key] = new List<string>();
            
            if (!dict[key].Contains(value))
                dict[key].Add(value);
        }

        private IReadOnlyList<Cve> ExecuteQueryWithCveArr(SqliteConnection connection, HashSet<string> cveArr, 
            bool throwOnError) 
            => Select(connection, AddCveArrayConstraint(cveArr), throwOnError);

        private IReadOnlyList<Cve> ExecuteQueryWithKeywords(SqliteConnection connection, string[] keywords, 
            DateTime? beginDate, DateTime? endDate, bool throwOnError)
        {
            var constraint = AddKeywordsConstraint(keywords);
            if (beginDate != null)
                constraint += AddBeginDateConstraint(beginDate.Value);
            
            if (endDate != null)
                constraint += AddEndDateConstraint(endDate.Value);

            return Select(connection, constraint, throwOnError);
        }
        
        

        private void EndDbCreating(SqliteConnection connection, FileInfo file)
        {
            logger.Log("Создание БД завершено");
            try
            {
                if (file.Exists)
                {
                    file.Delete();
                    logger.Debug($"Удален старый файл {file.Name}");
                }
                
                if (!file.Directory.Exists)
                    file.Directory.Create();
                
                using var backup = new SqliteConnection("Data Source=" + file.FullName);
                backup.Open();
                connection.BackupDatabase(backup);
                logger.Log($"БД сохранена в файл {file.FullName}");
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException($"Не удалось сохранить БД в файл {file.FullName}", e);
            }
        }

        protected void FillDbFromFile(SqliteConnection connection, FileInfo file, bool throwOnError)
        {
            logger.Log($"Чтение файла {file.Name}");
            using var fileReader = file.OpenText();
            ReadCveFileFromStream(fileReader, en =>
            {
                foreach (var cve in ParseJsonCve(en, throwOnError))
                {
                    try
                    {
                        AddCve(connection, cve);
                    }
                    catch (StopProgramException)
                    {
                        if (throwOnError)
                            throw;
                    }
                    catch (Exception e)
                    {
                        var msg = $"Ошибка при добавлении {cve.Id} в БД";
                        if (throwOnError)
                            throw logger.LogAndCreateStopProgramException(msg, e);
                        logger.Log(msg, LogSeverity.Error);
                    }
                }
            });
        }

        private void CreateDb(FileInfo dbFile, bool throwOnError)
        {
            try
            {
                logger.Log("Создание БД");
                using var connection = new SqliteConnection("Data Source=Sharable;Mode=Memory;Cache=Shared");
                connection.Open();
                CreateTables(connection);
                logger.Debug("Завершено создание основных таблиц");
                var dir = new DirectoryInfo(nvdFolder);
                if (!dir.Exists)
                    throw logger.LogAndCreateStopProgramException($"Не удалось обнаружить директорию {dir.FullName}");

                var files = dir.GetFiles(string.Format(nvdFileFormat, "*"));
                if (files.Length == 0)
                    throw logger.LogAndCreateStopProgramException(
                        $"Не удалось найти файлы, удовлетворяющие формату {nvdFileFormat}");
                
                StartExecution(connection, files, throwOnError);
                EndDbCreating(connection, dbFile);
            }
            catch (StopProgramException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException("Возникла ошибка при создании БД", e);
            }
            GC.Collect();
        }

        private IReadOnlyList<Cve> DoWork(Func<SqliteConnection, IReadOnlyList<Cve>> work, bool throwOnError)
        {
            const string dbName = "cve-db.db3";
            var dbFile = new FileInfo(Path.Join(tempFolder, dbName));
            if (!loadFromFile || !dbFile.Exists)
                CreateDb(dbFile, throwOnError);

            try
            {
                using var connection = new SqliteConnection("Mode=ReadOnly;Data Source=" + dbFile.FullName);
                connection.Open();
                logger.Log("Поиск уязвимостей в БД");
                var list = work(connection);
                logger.Log("Поиск в БД завершен");
                return list;
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException("Возникла ошибка при загрузке из БД", e);
            }
        }

        protected virtual void StartExecution(SqliteConnection connection, FileInfo[] files, bool throwOnError)
        {
            foreach (var file in files) 
                FillDbFromFile(connection, file, throwOnError);
        }

        public override IReadOnlyList<Cve> GetVulnerabilitiesFromList(HashSet<string> cveArr, bool throwOnError)
        {
            return DoWork(con => ExecuteQueryWithCveArr(con, cveArr, throwOnError), throwOnError);
        }

        public override IReadOnlyList<Cve> GetVulnerabilitiesFromKeywords(string[] keywords, bool throwOnError, 
            DateTime? beginDate, DateTime? endDate)
        {
            return DoWork(con => ExecuteQueryWithKeywords(con, keywords, beginDate, endDate, throwOnError),
                throwOnError);
        }
    }
}