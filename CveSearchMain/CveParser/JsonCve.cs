﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace CveSearchMain.CveParser
{
    public class JsonCve
    {
        [JsonProperty("publishedDate")]
        public DateTime? PublishedDate { get; set; }
        [JsonProperty("lastModifiedDate")]
        public DateTime? LastModifiedDate { get; set; }
        [JsonProperty("cve")]
        public CveEntry Cve { get; set; }
        [JsonProperty("impact")]
        public ImpactEntry Impact { get; set; }

        public Cve ToCve()
        {
            var v2 = Impact.BaseMetricV2.CvssV2.BaseScore ?? "N/A";
            var v3 = Impact.BaseMetricV3.CvssV3.BaseScore ?? "N/A";
            var impact = v2 == "N/A" && v3 == "N/A" ? "N/A" : $"{v2}:{v3}";
            var refs = HandleReferences();

            return new Cve()
            {
                Id = Cve.DataMeta.Id,
                CvssV2 = v2,
                CvssV3 = v3,
                Impact = impact,
                LastModifiedDate = LastModifiedDate ?? DateTime.MinValue,
                PublishedDate = PublishedDate ?? DateTime.MinValue,
                CweCollection = HandleCwe(),
                References = refs,
                PatchRefs = PatchRefManager.HandlePatches(refs),
                Description = HandleDescription()
            };
        }

        private IReadOnlyList<string> HandleReferences()
        {
            if (Cve.References.Data == null)
                return new List<string>();
            return Cve.References.Data
                .Select(r => r.Url)
                .Where(u => !string.IsNullOrEmpty(u))
                .ToList()!;
        }

        private string HandleDescription()
        {
            const string errMsg = "Описание не доступно.";
            var arr = Cve.Description.Data;
            if (arr == null)
                return errMsg;
            var firstLang = arr.FirstOrDefault(d => d.Lang == "ru").Value;
            firstLang ??= arr.FirstOrDefault(d => d.Lang == "en").Value;
            firstLang ??= arr.FirstOrDefault(d => d.Value != null).Value;
            return firstLang ?? errMsg;
        }

        private IReadOnlyList<string> HandleCwe()
        {
            var allCwe = Cve.ProblemType.Data?.Select(c => c.Cwe).ToList();
            var cwe = new List<string>();
            if (allCwe == null || allCwe.Count == 0)
                return cwe;

            foreach (var cweWithLang in allCwe)
            {
                if (cweWithLang == null || cweWithLang.Count == 0)
                    continue;
                
                if (cweWithLang.Count == 1)
                {
                    var val = cweWithLang[0].Value;
                    if (val != null)
                        cwe.Add(val);
                    continue;
                }

                var firstLang = cweWithLang.FirstOrDefault(c => c.Lang == "ru").Value;
                firstLang ??= cweWithLang.FirstOrDefault(c => c.Lang == "en").Value;
                firstLang ??= cweWithLang.FirstOrDefault(c => c.Value != null).Value;
                if (firstLang != null)
                    cwe.Add(firstLang);
            }

            return cwe;
        }

        public struct CveEntry
        {
            [JsonProperty("CVE_data_meta")]
            public DataMetaEntry DataMeta { get; set; }
            [JsonProperty("problemtype")]
            public ProblemTypeEntry ProblemType { get; set; }
            [JsonProperty("references")]
            public ReferencesEntry References { get; set; }
            [JsonProperty("description")]
            public DescriptionEntry Description { get; set; }

            public struct DataMetaEntry
            {
                [JsonProperty("ID")]
                public string Id { get; set; }
            }
            
            public struct ProblemTypeEntry
            {
                [JsonProperty("problemtype_data")]
                public List<DataEntry>? Data { get; set; }
                
                public struct DataEntry
                {
                    [JsonProperty("description")]
                    public List<CweEntry>? Cwe { get; set; }
                    
                    public struct CweEntry
                    {
                        [JsonProperty("lang")]
                        public string? Lang { get; set; }
                        [JsonProperty("value")]
                        public string? Value { get; set; }
                    }
                }
            }
            
            public struct ReferencesEntry
            {
                [JsonProperty("reference_data")]
                public List<RefEntry>? Data { get; set; }
                
                public struct RefEntry
                {
                    [JsonProperty("url")]
                    public string? Url { get; set; }
                }
            }
            
            public struct DescriptionEntry
            {
                [JsonProperty("description_data")]
                public List<DescEntry>? Data { get; set; }
                
                public struct DescEntry
                {
                    [JsonProperty("lang")]
                    public string? Lang { get; set; }
                    [JsonProperty("value")]
                    public string? Value { get; set; }
                }
            }
        }

        public struct ImpactEntry
        {
            [JsonProperty("baseMetricV3")]
            public BaseMetricV3Entry BaseMetricV3 { get; set; }
            [JsonProperty("baseMetricV2")]
            public BaseMetricV2Entry BaseMetricV2 { get; set; }

            public struct BaseMetricV3Entry
            {
                [JsonProperty("cvssV3")] 
                public CvssV3Entry CvssV3 { get; set; }

                public struct CvssV3Entry
                {
                    [JsonProperty("baseScore")]
                    public string? BaseScore { get; set; }
                }
            }

            public struct BaseMetricV2Entry
            {
                [JsonProperty("cvssV2")]
                public CvssV2Entry CvssV2 { get; set; }
                
                public struct CvssV2Entry
                {
                    [JsonProperty("baseScore")]
                    public string? BaseScore { get; set; }
                }
            }
        }
    }
}