﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CveSearchMain.CveParser
{
    public class Cve
    {
        public DateTime PublishedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Id { get; set; } = null!;
        public string Description { get; set; } = null!;
        public IReadOnlyList<string> CweCollection { get; set; } = null!;
        public IReadOnlyList<string> References { get; set; } = null!;
        public IReadOnlyList<string> PatchRefs { get; set; } = null!;
        public string Impact { get; set; } = null!;
        public string CvssV2 { get; set; } = null!;
        public string CvssV3 { get; set; } = null!;

        [SuppressMessage("ReSharper", "ConstantNullCoalescingCondition")]
        public bool Verify()
        {
            if (string.IsNullOrEmpty(Id))
                return false;
            if (string.IsNullOrEmpty(Description))
                return false;
            if (string.IsNullOrEmpty(Impact))
                return false;
            if (string.IsNullOrEmpty(CvssV2))
                return false;
            if (string.IsNullOrEmpty(CvssV3))
                return false;
            
            CweCollection ??= new List<string>();
            References ??= new List<string>();
            PatchRefs ??= new List<string>();
            return true;
        }
    }
}