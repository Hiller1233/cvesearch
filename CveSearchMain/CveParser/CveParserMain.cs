﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CveSearchMain.CveParser.CveSource;
using CveSearchMain.InputParser;
using CveSearchMain.Logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CveSearchMain.CveParser
{
    public class CveParserMain : IModule
    {
        private ArgsParser<Args>? parser;
        private readonly ILogger logger;

        public CveParserMain(ILogger logger)
        {
            this.logger = logger;
        }

        private static string DefaultNdvFolder => @"nvd";
        private static string DefaultTempFolder => @"temp";
        private static string DefaultFormat => @"nvdcve-1.1-{0}.json";
        private static string DefaultCveSource => "pdb";
        private static int DefaultResultsPerPage => 100;

        public IReadOnlyList<Cve> ReceiveVulnerabilities(Args args)
        {
            ValidateAndFixArgs(args);
            var cveSource = GetCveSource(args);
            var cveList = args.CveArray != null ? ProcessCveList(args.CveArray, args.StopProgramOnError) : null;
            if (cveList != null && cveList.Count == 0)
                throw logger.LogAndCreateStopProgramException("Не обнаружено CVE для поиска");

            var res = args.DoNotLoadCveFromFile 
                ? new List<Cve>() 
                : CompareWithTempFile(cveList!, args.TempFolder!, args.Keywords);
            var allCveLoaded = res.Count != 0 && (cveList != null && cveList.Count == 0 || cveList == null);
            if (!allCveLoaded)
            {
                if (args.Keywords == null)
                    res.AddRange(cveSource.GetVulnerabilitiesFromList(cveList!, args.StopProgramOnError));
                else
                {
                    res.Clear();
                    res.AddRange(cveSource.GetVulnerabilitiesFromKeywords(args.Keywords, args.StopProgramOnError, 
                        args.StartDate, args.EndDate));
                }
            }

            if (allCveLoaded)
                logger.Log("Все CVE загружены из временного файла");
            
            if (!args.DoNotPrintResultsToConsole)
                PrintToConsole(res);
            if (!args.DoNotSaveCveToFile && !allCveLoaded)
                SaveCveToFile(args.TempFolder!, args.Keywords, res);
            return res;
        }

        private ICveSource GetCveSource(Args args)
        {
            switch (args.CveSource)
            {
                case "file":
                    return new FileCveSource(args.NvdFolder!, args.FileNameFormat!);
                case "req":
                    return new NvdCveSource(args.ResultsPerPage!.Value);
                case "db":
                    return new SqliteCveSource(logger, args.FileNameFormat!, args.NvdFolder!, args.TempFolder!, 
                        !args.DoNotLoadDb);
                case "pdb":
                    return new ParallelSqliteCveSource(logger, args.FileNameFormat!, args.NvdFolder!, args.TempFolder!, 
                        !args.DoNotLoadDb);
                default:
                    throw new ArgumentException();
            }
        }

        private List<Cve> CompareWithTempFile(HashSet<string> cveDict, string resultFolder, string[]? keywords)
        {
            var fileName = keywords == null || keywords.Length == 0
                ? "temp-cve.json"
                : string.Join('-', keywords) + "-cve.json";
            var path = Path.Join(resultFolder, fileName);
            var file = new FileInfo(path);
            if (keywords == null)
                return CompareWithTempFileInternal(cveDict, file);
            logger.Log("Считывание результата последнего поиска");
            return LoadCveFromFile(file) ?? new List<Cve>();
        }
        
        private List<Cve> CompareWithTempFileInternal(HashSet<string> cveDict, FileInfo file)
        {
            logger.Log("Считывание из временного файла");
            var res = new List<Cve>();
            var loadedCve = LoadCveFromFile(file);
            if (loadedCve == null || loadedCve.Count == 0)
                return res;
            foreach (var cve in loadedCve)
            {
                if (!cveDict.Contains(cve.Id))
                    continue;
                cveDict.Remove(cve.Id);
                res.Add(cve);
            }

            return res;
        }

        private List<Cve>? LoadCveFromFile(FileInfo file)
        {
            if (!file.Exists)
            {
                logger.Log("Временный файл с CVE не обнаружен");
                return null;
            }
            try
            {
                return LoadCveFromFileInternal(file);
            }
            catch (StopProgramException)
            {
                
            }
            catch (Exception)
            {
                logger.Log($"Ошибка при открытии файла: {file.FullName}", LogSeverity.Error);
            }

            return null;
        }

        private List<Cve>? LoadCveFromFileInternal(FileInfo file)
        {
            using var reader = file.OpenText();
            using var jReader = new JsonTextReader(reader);
            try
            {
                var cveArr = JToken.ReadFrom(jReader)
                    .Children()
                    .ParsePureCve(logger)
                    .ToList();
                return cveArr;
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException($"Ошибка при считывании данных из файла {file.Name}", e);
            }
        }

        private void SaveCveToFile(string resultFolder, string[]? keywords, IReadOnlyList<Cve> cveArr)
        {
            logger.Log("Запись временного файла");
            var fileName = keywords == null || keywords.Length == 0
                ? "temp-cve.json"
                : string.Join('-', keywords) + "-cve.json";
            var path = Path.Join(resultFolder, fileName);
            var file = new FileInfo(path);
            try
            {
                SaveCveToFileInternal(file, cveArr);
            }
            catch (StopProgramException)
            {
                
            }
            catch (Exception)
            {
                logger.Log($"Ошибка при открытии файла: {file.FullName}", LogSeverity.Error);
            }
        }
        
        private void SaveCveToFileInternal(FileInfo file, IReadOnlyList<Cve> cveArr)
        {
            if (file.Directory != null && !file.Directory.Exists)
                file.Directory.Create();
            using var fileWriter = file.OpenWrite();
            using var writer = new StreamWriter(fileWriter);
            using var jWriter = new JsonTextWriter(writer);
            try
            {
                var serializer = new JsonSerializer {Formatting = Formatting.Indented};
                serializer.Serialize(jWriter, cveArr);
            }
            catch (Exception e)
            {
                throw logger.LogAndCreateStopProgramException($"Ошибка при сериализации данных в файл {file.Name}", e);
            }
        }

        private HashSet<string> ProcessCveList(IEnumerable<string> list, bool throwOnError)
        {
            var regex = new Regex(@"^(?:CVE-)?\d{4}-\d*$", RegexOptions.Singleline);
            var res = new HashSet<string>();
            foreach (var cve in list)
            {
                if (!regex.IsMatch(cve))
                {
                    var msg = $"Неверный формат CVE-ID: {cve}";
                    if (throwOnError)
                        throw logger.LogAndCreateStopProgramException(msg);
                    logger.Log(msg, LogSeverity.Error);
                    continue;
                }

                var temp = cve;
                if (!temp.StartsWith("CVE-"))
                    temp = "CVE-" + cve;
                res.Add(temp);
            }
            return res;
        }

        public void PrintToConsole(IEnumerable<Cve> cveList)
        {
            var sb = new StringBuilder();
            foreach (var cve in cveList)
            {
                sb.AppendLine($"= {cve.Id} === {cve.Impact} = {cve.LastModifiedDate} = {cve.PublishedDate}")
                    .AppendLine(cve.Description).AppendLine();
                foreach (var cwe in cve.CweCollection)
                    sb.AppendLine(cwe);
                foreach (var refer in cve.References)
                    sb.AppendLine(refer);
                Console.WriteLine(sb.ToString());
                sb.Clear();
            }
        }

        private void ValidateAndFixArgs(Args args)
        {
            if (args.Keywords != null && args.Keywords.Length == 0)
                args.Keywords = null;
            if (args.CveArray != null && args.CveArray.Length == 0)
                args.CveArray = null;
            if (args.Keywords != null)
                args.CveArray = null;
            else if (args.CveArray == null)
                throw logger.LogAndCreateStopProgramException("Для получения CVE нужно их перечислить или указать слова для поиска");
            args.NvdFolder ??= DefaultNdvFolder;
            args.TempFolder ??= DefaultTempFolder;
            args.FileNameFormat ??= DefaultFormat;
            try
            {
                var _ = string.Format(args.FileNameFormat, "2020");
            }
            catch (Exception)
            {
                throw logger.LogAndCreateStopProgramException("Задан неверный формат имени файла");
            }

            args.ResultsPerPage ??= DefaultResultsPerPage;
            if (args.ResultsPerPage > 5000 || args.ResultsPerPage < 10)
                throw logger.LogAndCreateStopProgramException("Значение получаемых CVE должно быть 10-5000");

            if (args.Keywords == null)
            {
                args.StartDate = null;
                args.EndDate = null;
            }

            args.CveSource ??= DefaultCveSource;
            if (args.CveSource != "file" && args.CveSource != "req" 
                                         && args.CveSource != "db" && args.CveSource != "pdb")
                throw logger.LogAndCreateStopProgramException("Источник cve имеет неверное значение");
        }

        public void ProcessArgs(string[] args)
        {
            parser ??= new ArgsParser<Args>();
            var parsedArgs = parser.ParseArgs(args);
            if (parsedArgs != null)
                ReceiveVulnerabilities(parsedArgs);
        }

        public class Args : IInputArgs
        {
            [CommandInfo("--do-not-save", "-ns", "не сохранять результаты в файл")]
            public bool DoNotSaveCveToFile { get; set; }
            
            [CommandInfo("--do-not-load", "-nl", "не загружать результаты из файла")]
            public bool DoNotLoadCveFromFile { get; set; }
            
            [CommandInfo("--stop-on-error", "-s", "при незначительной ошибке программа остановится")]
            public bool StopProgramOnError { get; set; }
            
            [CommandInfo("--do-not-print", "-np", "не выводить найденные cve в консоль")]
            public bool DoNotPrintResultsToConsole { get; set; }
            
            [CommandInfo("--cve-list", "-c", "список cve для поиска (должен быть указан последним)")]
            public string[]? CveArray { get; set; }
            
            [CommandInfo("--directory", "-d", "директория, в которой следует искать БД NVD")]
            public string? NvdFolder { get; set; }
            
            [CommandInfo("--temp-dir", "-td", "директория, в которой следует сохранять результаты")]
            public string? TempFolder { get; set; }
            
            [CommandInfo("--format", "-f", "формат имени файла БД NVD")]
            public string? FileNameFormat { get; set; }
            
            [CommandInfo("--results-per-page", "-rpp", "устанавливает желаемое число CVE на один запрос")]
            public int? ResultsPerPage { get; set; }
            
            [CommandInfo("--keywords", "-k", "задает ключевые слова для поиска (должен быть указан последним)")]
            public string[]? Keywords { get; set; }

            [CommandInfo("--date-start", "-ds", "устанавливает начальную дату поиска")]
            public DateTime? StartDate { get; set; }
            
            [CommandInfo("--date-end", "-de", "устанавливает конечную дату поиска")]
            public DateTime? EndDate { get; set; }
            
            [CommandInfo("--do-not-load-db", "-nldb", "не загружать готовую базу данных")]
            public bool DoNotLoadDb { get; set; }
            
            [CommandInfo("--cve-source", "-cs", "источник cve ([pdb], db, file, req)")]
            public string? CveSource { get; set; }
        }
    }
}