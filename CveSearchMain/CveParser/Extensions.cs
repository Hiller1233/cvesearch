﻿using System;
using System.Collections.Generic;
using CveSearchMain.Logger;
using Newtonsoft.Json.Linq;

namespace CveSearchMain.CveParser
{
    public static class Extensions
    {
        public static IEnumerable<Cve> ParsePureCve(this IEnumerable<JToken> tokens, ILogger logger)
        {
            foreach (var token in tokens)
            {
                Cve cve;
                try
                {
                    cve = token.ToObject<Cve>()!;
                     if (!cve.Verify())
                         throw new ArgumentException();
                }
                catch (Exception)
                {
                    var id = (string) token["Id"]!;
                    if (string.IsNullOrEmpty(id))
                        id = "CVE";
                    logger.Log("Ошибка при парсинге: " + id);
                    continue;
                }
                yield return cve;
            }
        }
    }
}