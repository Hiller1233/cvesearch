﻿namespace CveSearchMain
{
    public interface IModule
    {
        void ProcessArgs(string[] args);
    }
}