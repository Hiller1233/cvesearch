﻿using System;
using System.Collections.Concurrent;
using System.IO;
using CveSearchMain.Logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CveSearchMain.NvdDownloader
{
    public class PatchRequestCache
    {
        private readonly FileInfo? file;
        private readonly ConcurrentDictionary<string, string> dict;
        private int startLength;

        public PatchRequestCache(FileInfo? file)
        {
            this.file = file;
            startLength = 0;
            dict = new ConcurrentDictionary<string, string>();
        }

        public bool Contains(string linkToCommit)
        {
            return dict.ContainsKey(linkToCommit);
        }

        public string this[string linkToCommit]
        {
            get => dict[linkToCommit];
            set => dict.TryAdd(linkToCommit, value);
        }

        public void Save(ILogger logger)
        {
            if (startLength == dict.Count || file == null)
                return;

            try
            {
                if (file.Directory != null && !file.Directory.Exists)
                    file.Directory.Create();
            
                using var writer = file.OpenWrite();
                using var w = new StreamWriter(writer);
                using var jWriter = new JsonTextWriter(w);
                logger.Log("Начало записи в файл кэша запросов", LogSeverity.Debug);
                try
                {
                    var ser = new JsonSerializer {Formatting = Formatting.Indented};
                    jWriter.WriteStartArray();
                    foreach (var (key, value) in dict)
                    {
                        ser.Serialize(jWriter, (key, value));
                    }
                    jWriter.WriteEndArray();
                }
                catch (Exception)
                {
                    throw logger.LogAndCreateStopProgramException("Ошибка при сериализации данных");
                }
            }
            catch (StopProgramException)
            {
                return;
            }
            catch (Exception)
            {
                logger.Log("Произошла ошибка при записи в файл " + file.FullName, LogSeverity.Error);
                return;
            }
            
            logger.Log("Кэш запросов сохранен в файл " + file.FullName, LogSeverity.Success);
        }

        public static PatchRequestCache? Load(ILogger logger, FileInfo? file)
        {
            if (file == null)
                return null;
            
            if (!file.Exists)
            {
                logger.Log("Файл кэша запросов не найден");
                return null;
            }
            
            var cache = new PatchRequestCache(file);
            try
            {
                using var reader = file.OpenText();
                using var jReader = new JsonTextReader(reader);
                logger.Log("Считывание из файла кэша запросов", LogSeverity.Debug);
                try
                {
                    jReader.Read();
                    while (true)
                    {
                        jReader.Read();
                        if (jReader.TokenType == JsonToken.EndArray)
                            break;
                        
                        var (link, patch) = JToken.ReadFrom(jReader).ToObject<(string, string)>()!;
                        cache[link] = patch;
                    }
                }
                catch (Exception)
                {
                    throw logger.LogAndCreateStopProgramException("Ошибка при чтении данных", LogSeverity.Error);
                }
            }
            catch (StopProgramException)
            {
                return null;
            }
            catch (Exception)
            {
                logger.Log("Произошла ошибка при чтении из файла " + file.FullName, LogSeverity.Error);
                return null;
            }
            
            logger.Log("Кэш запросов загружен из файла " + file.FullName, LogSeverity.Success);
            cache.startLength = cache.dict.Count;
            return cache;
        }
    }
}