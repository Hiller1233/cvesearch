﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CveSearchMain.InputParser;
using CveSearchMain.Logger;

namespace CveSearchMain.NvdDownloader
{
    public class NvdDownloaderMain : IModule
    {
        static NvdDownloaderMain()
        {
            Client = new HttpClient();
        }

        public static HttpClient Client { get; }

        private readonly ArgsParser<Args> argsParser;
        private static PatchRequestCache? cache;
        private static string DefaultNdvFolder => @"nvd";
        private static int DefaultBeginYear => 2002;
        private static int DefaultEndYear => DateTime.Now.Year;

        public NvdDownloaderMain()
        {
            argsParser = new ArgsParser<Args>();
        }

        public Args? ProcessAndReturnArgs(string[] args) => argsParser.ParseArgs(args);

        public void DownloadFeed(int beginYear, int endYear)
        {
            DownloadFeed(new Args(beginYear, endYear));
        }

        public void DownloadFeed(Args args)
        {
            ValidateAndFixArgs(args);

            Console.WriteLine($"Получение данных о CVE с {args.BeginYear} года по {args.EndYear}");
            var list = new List<(string, string)>();
            var dir = new DirectoryInfo(args.NvdFolder);
            if (!dir.Exists)
                try
                {
                    dir.Create();
                }
                catch (Exception)
                {
                    throw new StopProgramException($"** Не удалось создать каталог {dir.FullName}");
                }
            for (var i = args.BeginYear; i <= args.EndYear; i++)
                list.Add(($"https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-{i}.json.zip", 
                    Path.Join(dir.FullName, $"{i}.zip")));
            DownloadFilesAsync(list).Wait();
            foreach (var (_, path) in list)
            {
                ZipFile.ExtractToDirectory(path, dir.FullName, true);
                File.Delete(path);
            }
        }
        
        private static void ValidateAndFixArgs(Args args)
        {
            args.NvdFolder ??= DefaultNdvFolder;
            args.BeginYear ??= DefaultBeginYear;
            args.EndYear ??= DefaultEndYear;
            if (args.BeginYear > args.EndYear || args.BeginYear < DefaultBeginYear || args.EndYear > 2100)
                throw new StopProgramException($"** Неверно указан начальный и конечный год " +
                                               $"({args.BeginYear}) - ({args.EndYear})");
        }

        private static async Task DownloadFilesAsync(IEnumerable<(string, string)> sources)
        {
            using var webClient = new WebClient {Proxy = null};
            foreach (var (source, filename) in sources)
            {
                Console.WriteLine($"Загрузка из: {source}");
                await webClient.DownloadFileTaskAsync(source, filename);
            }
        }

        public static void PrepareCache(ILogger logger, bool load, string[]? keywords, string folder)
        {
            if (cache != null)
            {
                cache.Save(logger);
                cache = null;
            }
            
            FileInfo? file = null;
            if (keywords != null)
            {
                var keyString = string.Join('-', keywords);
                var path = Path.Join(folder, $"{keyString}-requests.json");
                file = new FileInfo(path);
            }
            
            if (load)
                cache = PatchRequestCache.Load(logger, file);
            
            cache ??= new PatchRequestCache(file);
        }
        
        public static void SaveCache(ILogger logger)
        {
            cache?.Save(logger);
        }
        
        public static string DownloadDataFromLinkCustom(ILogger logger, string link)
        {
            if (cache == null)
                return DownloadDataFromLink(logger, link, true);

            string content;
            if (cache.Contains(link))
                content = cache[link];
            else
            {
                content = DownloadDataFromLink(logger, link, true);
                cache[link] = content;
            }

            return content;
        }
        
        public static string DownloadDataFromLink(ILogger logger, string link)
            => DownloadDataFromLink(logger, link, false);

        public static string DownloadDataFromLink(ILogger logger, string link, bool sleepBeforeRequest)
        {
            if (sleepBeforeRequest)
                Thread.Sleep(50);
            var sleepTime = 10;
            for (var i = 0; i < 3; i++, sleepTime += 20)
            {
                using var res = Client.GetAsync(link).Result;
                if (res.IsSuccessStatusCode)
                    return res.Content.ReadAsStringAsync().Result;
            
                if (res.StatusCode != HttpStatusCode.TooManyRequests)
                    throw logger.LogAndCreateStopProgramException($"Не удалось загрузить данные с {link}");
                
                logger.Log($"При получении данных по ссылке {link} от сервера был получен ответ о превышении " +
                           $"количества запросов. Выполнение задачи будет приостановлено на {sleepTime} секунд.",
                    LogSeverity.RuntimeInfo);
                Thread.Sleep(1000 * sleepTime);
            }
            throw logger.LogAndCreateStopProgramException("Сервер вернул ответ о превышении количества запросов");
        }

        public void ProcessArgs(string[] args)
        {
            var parsedArgs = ProcessAndReturnArgs(args);
            if (parsedArgs != null)
                DownloadFeed(parsedArgs);
        }

        public class Args : IInputArgs
        {
            public Args()
            {
                
            }
            
            public Args(int beginYear, int endYear)
            {
                BeginYear = beginYear;
                EndYear = endYear;
            }
            
            [CommandInfo("--from", "-f", "Начальный год")]
            public int? BeginYear { get; set; }
            [CommandInfo("--to", "-t", "Конечный год")]
            public int? EndYear { get; set; }
            [CommandInfo("--dir", "-d", "директория, в которой следует сохранить БД NVD")]
            public string? NvdFolder { get; set; }
        }
    }
}