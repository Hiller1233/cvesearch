﻿using System;
using CveSearchMain.CommitSearch;
using CveSearchMain.CveAnalyzer;
using CveSearchMain.CveParser;
using CveSearchMain.InputParser;
using CveSearchMain.Logger;
using CveSearchMain.NvdDownloader;

namespace CveSearchMain
{
    public class MainDispatcher
    {
        private readonly ArgsParser argsParser;
        private bool isInteractive;
        private bool? isExitRequired;

        public MainDispatcher()
        {
            isInteractive = false;
            isExitRequired = false;
            
            var logger = new ConsoleLogger();
            
            var cveAnalyzer = new CveAnalyzerMain(logger);
            var nvdDownloader = new NvdDownloaderMain();
            var commitSearch = new CommitSearchMain(logger);
            var cveParser = new CveParserMain(logger);
            
            var commands = new CommandCollection(
                ArgCommandFactory.Create("analyze", "a", "меню выполнения задач", cveAnalyzer.ProcessArgs),
                ArgCommandFactory.Create("download", "d", "меню загрузки с NVD", nvdDownloader.ProcessArgs),
                ArgCommandFactory.Create("cve", "c", "меню поиска cve", cveParser.ProcessArgs),
                ArgCommandFactory.Create("commit", "co", "меню поиска коммита", commitSearch.ProcessArgs),
                ArgCommandFactory.Create("interactive", "i", "вход в интерактивный режим", Interactive),
                ArgCommandFactory.Create("debug", "dbg", "выполнение команды в режиме отладки", ToggleDebug),
                ArgCommandFactory.Create("quit", "q", "завершить работу программы", a => { isExitRequired = true; })
            );
            
            argsParser = new ArgsParser(commands);
        }

        public void Start(string[] args) => argsParser.ParseArgsRaw(args);

        private void Interactive(string[] nullArgs)
        {
            if (isInteractive)
            {
                Console.WriteLine("Интерактивный режим уже запущен.");
                return;
            }

            isInteractive = true;
            Console.WriteLine("Вход в интерактивный режим. Для выхода из него введите q.");
            while (true)
            {
                if (isExitRequired == true)
                    break;
                Console.Write(">> ");
                var args = Console.ReadLine()?.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    argsParser.ParseArgsRaw(args!);
                }
                catch (StopProgramException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private void ToggleDebug(string[] args)
        {
            if (Program.IsDebug)
                Console.WriteLine("Режим отладки уже включен");
            else
            {
                Program.DefineDebug();
                Console.WriteLine("Включен режим отладки");
            }

            Start(args);
        }
    }
}